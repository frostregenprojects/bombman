/**
 * KeyMapping.java 
 * Created on 12.05.2009
 * by Heiko Schmitt
 */
package bombman.input;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

import org.lwjgl.input.Controllers;
import org.lwjgl.input.Keyboard;

import bombman.debug.Debug;
import bombman.debug.Debug.Area;
import bombman.game.Controllable;

/**
 * @author Heiko Schmitt
 *
 */
public class KeyMapping
{
	private static String keymapFile = "keymap.ini";
	
	public static final int MAX_PLAYERS = 100;
	public static final int BUTTON_PRESSED = (1 << 8);
	public static final int AXIS_MINUS = (1 << 8);
	public static final int AXIS_PLUS = (2 << 8);
	
	private Controllable[] players;
	private Hashtable<Integer, CommandPIDMapping> commandMap;
	
	public enum Commands
	{
		WALK_X_LEFT,
		WALK_X_RIGHT,
		WALK_X_STOP,
		WALK_X_LEFT_STOP,
		WALK_X_RIGHT_STOP,
		
		WALK_Y_UP,
		WALK_Y_DOWN,
		WALK_Y_STOP,
		WALK_Y_UP_STOP,
		WALK_Y_DOWN_STOP,
		
		BOMB_BUTTON_PRESS,
		BOMB_BUTTON_RELEASE,
		SPECIAL_BUTTON_PRESS,
		SPECIAL_BUTTON_RELEASE
	}
	
	private Commands[] stopButtonCommand =
	{
			Commands.WALK_X_LEFT_STOP,
			Commands.WALK_X_RIGHT_STOP,
			null,
			null,
			null,
			
			Commands.WALK_Y_UP_STOP,
			Commands.WALK_Y_DOWN_STOP,
			null,
			null,
			null,
			
			null,
			null,
			null,
			null
	};
	
	private Commands[] stopAxisCommand =
	{
			Commands.WALK_X_STOP,
			Commands.WALK_X_STOP,
			null,
			null,
			null,
			
			Commands.WALK_Y_STOP,
			Commands.WALK_Y_STOP,
			null,
			null,
			null,
			
			null,
			null,
			null,
			null
	};
	
	/**
	 * 
	 */
	public KeyMapping()
	{
		players = new Controllable[MAX_PLAYERS];
		commandMap = new Hashtable<Integer, CommandPIDMapping>();
	}
	
	public void init()
	{
		try
		{
			Controllers.create();
		}
		catch (Exception e)
		{
			Debug.out(Area.General, 2, "Could not create Controllers. Maybe no gamepad plugged in.");
		}
		
		setupDefaults();
	}
	
	private void setupDefaults()
	{
		if (!loadKeymapFile(new File(keymapFile)))
		{
			for (int i = 1; i < 64; i++)
			{
				addMapping(getEventID(i, 0, 1, false), Commands.BOMB_BUTTON_PRESS, i);
				addMapping(getEventID(i, 1, 1, false), Commands.SPECIAL_BUTTON_PRESS, i);
				addMapping(getEventID(i, 1, -1, true), Commands.WALK_X_LEFT, i);
				addMapping(getEventID(i, 1, 1, true), Commands.WALK_X_RIGHT, i);
				addMapping(getEventID(i, 0, -1, true), Commands.WALK_Y_UP, i);
				addMapping(getEventID(i, 0, 1, true), Commands.WALK_Y_DOWN, i);
			}
			
			//Keyboard setup for one player
			addMapping(getEventID(0, Keyboard.KEY_B, 1, false), Commands.BOMB_BUTTON_PRESS, 0);
			addMapping(getEventID(0, Keyboard.KEY_V, 1, false), Commands.SPECIAL_BUTTON_PRESS, 0);
			addMapping(getEventID(0, Keyboard.KEY_LEFT, 1, false), Commands.WALK_X_LEFT, 0);
			addMapping(getEventID(0, Keyboard.KEY_RIGHT, 1, false), Commands.WALK_X_RIGHT, 0);
			addMapping(getEventID(0, Keyboard.KEY_UP, 1, false), Commands.WALK_Y_UP, 0);
			addMapping(getEventID(0, Keyboard.KEY_DOWN, 1, false), Commands.WALK_Y_DOWN, 0);
		}
	}
	
	public void addMapping(int eventID, Commands command, int playerID)
	{
		int releaseEventID = getReleaseEventID(eventID);
		if (isAxisEvent(eventID))
		{
			//Controller Axis
			commandMap.put(eventID, new CommandPIDMapping(command, playerID));
			//Add corresponding "stop" event->command mapping
			if (stopAxisCommand[command.ordinal()] != null)
				commandMap.put(releaseEventID, new CommandPIDMapping(stopAxisCommand[command.ordinal()], playerID));
		}
		else
		{
			//Button or Keyboard press
			commandMap.put(eventID, new CommandPIDMapping(command, playerID));
			//Add corresponding "stop" event->command mapping
			if (stopButtonCommand[command.ordinal()] != null)
				commandMap.put(releaseEventID, new CommandPIDMapping(stopButtonCommand[command.ordinal()], playerID));
		}
	}
	
	public int getReleaseEventID(int eventID)
	{
		return (eventID & 0xFFFF00FF);
	}
	
	private boolean isAxisEvent(int eventID)
	{
		return ((eventID & 0xFF) > 0) && (((eventID >> 24) & 0xFF) > 0);
	}
	
	public void registerControllable(int pid, Controllable player)
	{
		players[pid] = player;
	}
	
	public void processEvent(int eventID)
	{
		if (commandMap.containsKey(eventID))
		{
			CommandPIDMapping cpid = commandMap.get(eventID);
			players[cpid.target].processCommand(cpid.command);
		}
	}
	
	/**
	 * Generate eventID.<BR>
	 * keyID: Keyboard-KeyID, or ButtonID or AxisID<BR>
	 * keyState:<BR>
	 * Keyboard/Button: 0 released, 1 pressed
	 * Axis: 0 middle, 1 Plus, -1 Minus  
	 * @param controllerID
	 * @return
	 */
	public int getEventID(int controllerID, int keyID, int keyState, boolean isAxis)
	{
		keyID++; //Avoid 0 keyID
		
		int eventID = controllerID;
		if (controllerID == 0)
		{
			//Keyboard
			eventID = eventID | (keyID << 16);
			eventID = eventID | ((keyState == 1) ? KeyMapping.BUTTON_PRESSED : 0);
		}
		else
		{
			if (isAxis)
			{
				eventID = eventID | (keyID << 24);
				eventID = eventID | (keyState < 0 ? KeyMapping.AXIS_MINUS : (keyState > 0 ? KeyMapping.AXIS_PLUS : 0));
			}
			else
			{
				eventID = eventID | (keyID << 16);
				eventID = eventID | ((keyState == 1) ? KeyMapping.BUTTON_PRESSED : 0);
			}
		}
		return eventID;
	}
	
	public void dispose()
	{
		Controllers.destroy();
	}
	
	private boolean loadKeymapFile(File kmFile)
	{
		if (kmFile.exists())
		{
			try
			{
				//Read in Keymap
				BufferedReader br = new BufferedReader(new FileReader(kmFile));
				String line = br.readLine();
				String[] splittedLine;
				int currentLine = 1;
				while (line != null)
				{
					splittedLine = line.split("\t");
					if (splittedLine.length == 3)
					{
						try
						{
							addMapping(Integer.parseInt(splittedLine[0]), Commands.valueOf(splittedLine[1]), Integer.parseInt(splittedLine[2]));
						}
						catch (NumberFormatException e)
						{
							Debug.out(Area.Input, 3, "Error parsing keymap line, ignoring line: " + currentLine);
						}
					}
					line = br.readLine();
					currentLine++;
				}
				br.close();
				return true;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
}
