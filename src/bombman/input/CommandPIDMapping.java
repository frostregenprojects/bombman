/**
 * PlayerCommandMapping.java 
 * Created on 12.05.2009
 * by Heiko Schmitt
 */
package bombman.input;

import bombman.input.KeyMapping.Commands;

/**
 * @author Heiko Schmitt
 *
 */
public final class CommandPIDMapping
{
	public Commands command;
	public int target;
	
	public CommandPIDMapping(Commands command, int target)
	{
		this.command = command;
		this.target = target;
	}
}
