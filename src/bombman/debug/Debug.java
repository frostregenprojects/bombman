/**
 * Debug.java 
 * Created on 15.11.2008
 * by Heiko Schmitt
 */
package bombman.debug;

import java.io.PrintStream;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

/**
 * IMPORTANT:<BR>
 * Call setGlobalDebugLevel before first usage!!
 * @author Heiko Schmitt
 */
public class Debug
{
	private static Debug _instance = null;
	
	private ArrayList<PrintStream> outStreams;
	private int[] levels; 
	private DateFormat formatter;
	private StringBuilder builder;
	
	private Debug()
	{
	}

	private static void checkInstance()
	{
		if (_instance == null)
		{
			_instance = new Debug();
			_instance.outStreams = new ArrayList<PrintStream>();
			_instance.outStreams.add(System.out);
			
			_instance.formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
			_instance.builder = new StringBuilder();
			
			int count = Area.values().length;
			_instance.levels = new int[count];
			for (int i = 0; i < count; i++)
			{
				_instance.levels[i] = 10;
			}
		}
	}
	
	public enum Area
	{
		General,
		Game,
		Render,
		Sound,
		Network,
		Input,
	}
	
	/**
	 * IMPORTANT:<BR>
	 * CALL this method before using Debug!
	 * @param level
	 */
	public static void setGlobalDebugLevel(int level)
	{
		checkInstance();
		
		Area[] values = Area.values();
		for (int i = 0; i < values.length; i++)
		{
			_instance.levels[i] = level;
		}
	}
	
	private static String getTime()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(_instance.formatter.format(new Date()));
		sb.append("]");
		return sb.toString();
	}
	
	public static boolean checkLevel(Area area, int level)
	{
		return (level <= _instance.levels[area.ordinal()]);
	}
	
	/**
	 * Print a message to all registered Printstreams.<BR>
	 * This is currently NOT threadsafe!
	 * @param area
	 * @param level
	 * @param message
	 */
	public static void out(Area area, int level, String message)
	{
		if (checkLevel(area, level))
		{
			_instance.builder.setLength(0);
			_instance.builder.append(getTime());
			_instance.builder.append(" #");
			_instance.builder.append(area.toString());
			_instance.builder.append("# ");
			_instance.builder.append(message);
			
			Iterator<PrintStream> it = _instance.outStreams.iterator();
			while (it.hasNext())
			{
				it.next().println(_instance.builder.toString());
			}
		}
	}
}
