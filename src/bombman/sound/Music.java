/**
 * Music.java 
 * Created on 15.05.2009
 * by Heiko Schmitt
 */
package bombman.sound;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

import bombman.debug.Debug;
import bombman.debug.Debug.Area;

/**
 * @author Heiko Schmitt
 *
 */
public class Music
{
	public boolean loop;
	private String filename;
	
	private IntBuffer buffers;
	
	/* Data */
	private boolean dataRead;
	private byte[] data;
	private OggInputStream oggStream;
	
	//OpenAL upload buffer
	private ByteBuffer dataBuffer;
	public int inSource;
	
	/**
	 * 
	 */
	public Music(String filename)
	{
		this.loop = false;
		this.dataRead = false;
		this.filename = filename;
		this.buffers = BufferUtils.createIntBuffer(2);
		this.dataBuffer = BufferUtils.createByteBuffer(1024 * 128);
	}

	public void play(int inSource)
	{
		if (!dataRead) readData();
		
		oggStream = new OggInputStream(new ByteArrayInputStream(data));
		for (int i = 0; i < buffers.capacity(); i++)
		{
			if (!fillBuffer(buffers.get(i))) return;
		}
		
		this.inSource = inSource;
		AL10.alSourceQueueBuffers(inSource, buffers);
		AL10.alSourcePlay(inSource);
	}
	
	/**
	 * Returns if more updates are needed
	 * @return
	 */
	public boolean update()
	{
		boolean result = true;
		
		int processed = AL10.alGetSourcei(inSource, AL10.AL_BUFFERS_PROCESSED);
		while (processed-- > 0) {
			IntBuffer buffer = BufferUtils.createIntBuffer(1);
			AL10.alSourceUnqueueBuffers(inSource, buffer);
	
			result = fillBuffer(buffer.get(0));
			buffer.rewind();
	
			AL10.alSourceQueueBuffers(inSource, buffer);
			
			if (!result && loop)
			{
				//Restart from beginning
				oggStream = new OggInputStream(new ByteArrayInputStream(data));
				result = true;
			}
		}

		return result;
	}
	
	protected boolean fillBuffer(int buffer)
	{
		try 
		{
			dataBuffer.limit(dataBuffer.capacity());
			int bytesRead = oggStream.read(dataBuffer, 0, dataBuffer.capacity());
			if (bytesRead >= 0)
			{
				dataBuffer.rewind();
				dataBuffer.limit(bytesRead);
				boolean mono = (oggStream.getFormat() == OggInputStream.FORMAT_MONO16);
				int format = (mono ? AL10.AL_FORMAT_MONO16 : AL10.AL_FORMAT_STEREO16);
				AL10.alBufferData(buffer, format, dataBuffer, oggStream.getRate());
				return (bytesRead == dataBuffer.capacity());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	private void readData()
	{
		try
		{
			//Load Music data
			File file = new File(filename);
			int fileLen = (int)file.length();
			data = new byte[fileLen];
			FileInputStream fis = new FileInputStream(file);
			if (fis.read(data) != fileLen)
				Debug.out(Area.Sound, 1, "Loading music: could not load entire data...");
			dataRead = true;
			
			//Create OpenAL Buffers
			buffers.position(0);
			AL10.alGenBuffers(buffers);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
}
