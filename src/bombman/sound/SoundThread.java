/**
 * SoundThread.java 
 * Created on 14.05.2009
 * by Heiko Schmitt
 */
package bombman.sound;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

import bombman.debug.Debug;
import bombman.debug.Debug.Area;

/**
 * @author Heiko Schmitt
 *
 */
public class SoundThread extends Thread
{
	private static final String baseSoundPath = "res/sound/";
	private final int MAX_SOUNDS;
	
	//ID->Soundfile mapping
	private Sound[] sounds;
	
	//Sources (playback channels) (contains OpenAL IDs)
	private IntBuffer sources;
	
	//Playing sources list (contains sourceID)
	private List<Integer> playingSources;
	
	//Free sources (contains sourceID)
	private Queue<Integer> freeSources;
	
	//Commands
	private Queue<Integer> commandQueue;
	
	public static final int CMD_PLAY = 1;
	public static final int CMD_STOP = 2;
	public static final int CMD_LOAD = 3;
	public static final int CMD_STOP_ALL = 4;
	public static final int CMD_TERMINATE = 5;
	
	//Thread state:
	private boolean terminate;
	
	/**
	 * 
	 */
	public SoundThread(int maxPlayingSounds)
	{
		super("SoundThread");
		MAX_SOUNDS = maxPlayingSounds;
		terminate = false;
		
		init();
	}
	
	private void init()
	{
		playingSources = new LinkedList<Integer>();
		commandQueue = new ConcurrentLinkedQueue<Integer>();
		
		readSoundFile(baseSoundPath + "soundtable.ini");
		
		//Create Sources
		sources = BufferUtils.createIntBuffer(MAX_SOUNDS);
		AL10.alGenSources(sources);
		freeSources = new ArrayBlockingQueue<Integer>(MAX_SOUNDS);
		for (int i = 0; i < MAX_SOUNDS; i++)
			freeSources.offer(i);
	}

	/**
	 * Do something with this SoundPlayer:<BR>
	 * Use the predefined constants for PLAY/STOP/TERMINATE,<BR>
	 * together with sound number as param<BR>
	 * Typical usage would be:<BR>
	 * <BR>
	 * soundThread.doCommand(1, SoundThread.CMD_PLAY, 5); //Play sound number 5<BR>
	 * The first parameter is used as an identifier to this Play command.<BR>
	 * If you want to stop a sound, you have to supply this id as param<BR>
	 * soundThread.doCommand(0, SoundThread.CMD_STOP, 1); //Stop sound with id 1<BR>
	 * 
	 * @param cmd
	 */
	public void doCommand(int id, int cmd, int param)
	{
		commandQueue.offer((cmd & 0xFF) + ((id & 0xFF) << 8) + ((param & 0xFFFF) << 16));
		synchronized(this)
		{
			notify();
		}
	}
	
	@Override
	public void run()
	{
		while (!terminate)
		{
			//Check for new commands
			processCommands();
			
			//Work or sleep
			if (isWorkToDo())
				processWork();
			else
				try { synchronized(this) { wait(); } } catch (InterruptedException e) { }
		}
	}
	
	private boolean isWorkToDo()
	{
		return (terminate);
	}
	
	private void processWork()
	{
		
	}
	
	private void processCommands()
	{
		while (!commandQueue.isEmpty())
		{
			int cmd = commandQueue.poll();
			int id = (cmd >> 8) & 0xFF;
			int param = (cmd >> 16) & 0xFFFF;
			cmd = (cmd & 0xFF);
			
			switch (cmd)
			{
				case CMD_PLAY:
				{
					playSound(id, param);
					break;
				}
				case CMD_STOP:
				{
					stopSound(param);
					break;
				}
				case CMD_LOAD:
				{
					loadSound(param);
					break;
				}
				case CMD_STOP_ALL:
				{
					//
					break;
				}
				case CMD_TERMINATE:
				{
					terminate = true;
					break;
				}
				default: break;
			}
		}
	}
	
	private void checkFinishedSounds()
	{
		Iterator<Integer> it = playingSources.iterator();
		int id;
		while (it.hasNext())
		{
			id = it.next();
			if (AL10.alGetSourcei(sources.get(id), AL10.AL_SOURCE_STATE) != AL10.AL_PLAYING)
			{
				//Source is free
				it.remove();
				freeSources.offer(id);
			}
		}
	}
	
	private void playSound(int id, int soundID)
	{
		checkFinishedSounds();
		
		if (sounds[soundID] != null)
		{
			//Get free Buffer
			Integer freeBufferID = freeSources.poll();
			if (freeBufferID != null)
			{
				//Mark source as playing
				playingSources.add(freeBufferID);
				
				//Connect buffer to free source
				AL10.alSourcei(sources.get(freeBufferID), AL10.AL_BUFFER, sounds[soundID].getSoundBuffer());
				
				//Play Source
				AL10.alSourcePlay(sources.get(freeBufferID));
			}
		}
	}
	
	private void stopSound(int id)
	{
		
	}
	
	private void loadSound(int soundID)
	{
		
	}
	
	private void readSoundFile(String soundFile)
	{
		try
		{
			LinkedHashMap<Integer, String> soundFileMap = new LinkedHashMap<Integer, String>();
			
			//Read in SoundFiles
			int sndCount = 0;
			BufferedReader br = new BufferedReader(new FileReader(soundFile));
			String line = br.readLine();
			String[] splittedLine;
			int curID;
			while (line != null)
			{
				splittedLine = line.split("\t");
				if (splittedLine.length == 2)
				{
					curID = Integer.parseInt(splittedLine[0]);
					if (!soundFileMap.containsKey(curID))
					{
						soundFileMap.put(curID, splittedLine[1]);
					}
					else
					{
						Debug.out(Area.Sound, 3, "soundTable: ID: " + splittedLine[0] + " is already used! ignoring: " + splittedLine[1]);
					}
					if (curID >= sndCount)
						sndCount = curID + 1;
				}
				line = br.readLine();
			}
			br.close();
			
			//copy to mapping array
			sounds = new Sound[sndCount];
			Iterator<Integer> it = soundFileMap.keySet().iterator();
			while (it.hasNext())
			{
				int i = it.next();
				sounds[i] = new Sound(baseSoundPath + soundFileMap.get(i));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
