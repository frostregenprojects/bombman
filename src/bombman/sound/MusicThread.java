/**
 * MusicThread.java 
 * Created on 14.05.2009
 * by Heiko Schmitt
 */
package bombman.sound;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;

import bombman.debug.Debug;
import bombman.debug.Debug.Area;

/**
 * @author Heiko Schmitt
 *
 */
public class MusicThread extends Thread
{
	private static final String baseMusicPath = "res/music/";
	private final int MAX_MUSICS;
	
	//ID -> Musicfile mapping
	private Music[] musics;
	
	//Sources
	private IntBuffer sources;
	
	//Playing musics list (contains musicID)
	private List<Integer> playingMusics;
	
	//Free sources (contains sourceID)
	private Queue<Integer> freeSources;
	
	//Commands
	private Queue<Integer> commandQueue;
	
	public static final int CMD_PLAY = 1;
	public static final int CMD_LOOP = 2;
	public static final int CMD_STOP = 3;
	public static final int CMD_LOAD = 4;
	public static final int CMD_STOP_ALL = 5;
	public static final int CMD_TERMINATE = 6;
	
	//Params
	public static final int TRUE = 1;
	public static final int FALSE = 2;
	
	//Thread state:
	private boolean terminate;
	
	/**
	 * 
	 */
	public MusicThread(int maxPlayingMusics)
	{
		super("MusicThread");
		MAX_MUSICS = maxPlayingMusics;
		terminate = false;
		
		init();
	}

	private void init()
	{
		playingMusics = new LinkedList<Integer>();
		commandQueue = new ConcurrentLinkedQueue<Integer>();
		
		readMusicFile(baseMusicPath + "musictable.ini");
		
		//Create sources
		sources = BufferUtils.createIntBuffer(MAX_MUSICS);
		AL10.alGenSources(sources);
		freeSources = new ArrayBlockingQueue<Integer>(MAX_MUSICS);
		for (int i = 0; i < MAX_MUSICS; i++)
			freeSources.offer(i);
	}
	
	private void playMusic(int id, int musicID)
	{
		//checkFinishedSounds();
		
		if (musics[musicID] != null)
		{
			//Get free Buffer
			Integer freeBufferID = freeSources.poll();
			if (freeBufferID != null)
			{
				//Mark source as playing
				playingMusics.add(musicID);
				
				//Play
				musics[musicID].play(sources.get(freeBufferID));
			}
		}
	}
	
	/**
	 * Do something with this SoundPlayer:<BR>
	 * Use the predefined constants for PLAY/STOP/TERMINATE,<BR>
	 * together with sound number as param<BR>
	 * Typical usage would be:<BR>
	 * <BR>
	 * soundThread.doCommand(1, SoundThread.CMD_PLAY, 5); //Play sound number 5<BR>
	 * The first parameter is used as an identifier to this Play command.<BR>
	 * If you want to stop a sound, you have to supply this id as param<BR>
	 * soundThread.doCommand(0, SoundThread.CMD_STOP, 1); //Stop sound with id 1<BR>
	 * 
	 * @param cmd
	 */
	public void doCommand(int id, int cmd, int param)
	{
		commandQueue.offer((cmd & 0xFF) + ((id & 0xFF) << 8) + ((param & 0xFFFF) << 16));
		synchronized(this)
		{
			notify();
		}
	}
	
	@Override
	public void run()
	{
		while (!terminate)
		{
			//Check for new commands
			processCommands();
			
			//Work or sleep
			if (isWorkToDo())
			{
				processWork();
				try { sleep(1); } catch (InterruptedException e) { }
			}
			else
				try { synchronized(this) { wait(); } } catch (InterruptedException e) { }
		}
	}
	
	private boolean isWorkToDo()
	{
		return (terminate || !playingMusics.isEmpty());
	}
	
	private void processWork()
	{
		if (terminate) return;
		
		Iterator<Integer> it = playingMusics.iterator();
		int id;
		while (it.hasNext())
		{
			id = it.next();
			if (!musics[id].update())
			{
				//finished
				freeSources.offer(musics[id].inSource);
				it.remove();
			}
		}
	}
	
	private void processCommands()
	{
		while (!commandQueue.isEmpty())
		{
			int cmd = commandQueue.poll();
			int id = (cmd >> 8) & 0xFF;
			int param = (cmd >> 16) & 0xFFFF;
			cmd = (cmd & 0xFF);
			
			switch (cmd)
			{
				case CMD_PLAY:
				{
					playMusic(id, param);
					break;
				}
				case CMD_LOOP:
				{
					musics[1].loop = (param == TRUE); //TODO incorporate ID management...
					break;
				}
				case CMD_STOP:
				{
					//stopMusic(param);
					break;
				}
				case CMD_LOAD:
				{
					//loadMusic(param);
					break;
				}
				case CMD_STOP_ALL:
				{
					//
					break;
				}
				case CMD_TERMINATE:
				{
					terminate = true;
					break;
				}
				default: break;
			}
		}
	}

	private void readMusicFile(String musicFile)
	{
		try
		{
			LinkedHashMap<Integer, String> musicFileMap = new LinkedHashMap<Integer, String>();
			
			//Read in MusicFiles
			int sndCount = 0;
			BufferedReader br = new BufferedReader(new FileReader(musicFile));
			String line = br.readLine();
			String[] splittedLine;
			int curID;
			while (line != null)
			{
				splittedLine = line.split("\t");
				if (splittedLine.length == 2)
				{
					curID = Integer.parseInt(splittedLine[0]);
					if (!musicFileMap.containsKey(curID))
					{
						musicFileMap.put(curID, splittedLine[1]);
					}
					else
					{
						Debug.out(Area.Sound, 3, "musicTable: ID: " + splittedLine[0] + " is already used! ignoring: " + splittedLine[1]);
					}
					if (curID >= sndCount)
						sndCount = curID + 1;
				}
				line = br.readLine();
			}
			br.close();
			
			//copy to mapping array
			musics = new Music[sndCount];
			Iterator<Integer> it = musicFileMap.keySet().iterator();
			while (it.hasNext())
			{
				int i = it.next();
				musics[i] = new Music(baseMusicPath + musicFileMap.get(i));
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
