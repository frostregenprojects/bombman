/**
 * Sound.java 
 * Created on 14.05.2009
 * by Heiko Schmitt
 */
package bombman.sound;

import java.io.FileInputStream;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

/**
 * @author Heiko Schmitt
 *
 */
public class Sound
{
	private String filename;
	public boolean dataBuffered; 
	
	//Buffers (contains data)
	private IntBuffer buffer;
	
	/**
	 * 
	 */
	public Sound(String filename)
	{
		this.filename = filename;
		this.dataBuffered = false;
		this.buffer = BufferUtils.createIntBuffer(1);
	}

	public int getSoundBuffer()
	{
		if (!dataBuffered)
		{
			try
			{
				buffer.position(0);
				AL10.alGenBuffers(buffer);
				
				//load wavedata
				FileInputStream fis = new FileInputStream(filename);
				WaveData waveFile = WaveData.create(fis);
				
				//copy to buffer
				AL10.alBufferData(buffer.get(0), waveFile.format, waveFile.data, waveFile.samplerate);
				
				waveFile.dispose();
				fis.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		return buffer.get(0);
	}
	
}
