/**
 * SoundManager.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.sound;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC10;

import bombman.application.Settings;
import bombman.debug.Debug;
import bombman.debug.Debug.Area;

/**
 * @author Heiko Schmitt
 *
 */
public class SoundManager
{
	//Settings
	private static Settings settings;
	
	//Players
	private static MusicThread musicThread;
	private static SoundThread soundThread;
	
	/*
	 * NOTE: The current implementation with ID's is not very reliable.
	 * Once a sound has finished, it should return the used ID for subsequent usage.
	 * This is currently not done, because we never need to stop a sound,
	 * and rarely need to stop the music (There is only one track playing at a time,
	 * so no risk of ID overwriting...)
	 * For a fully generic SoundManager this NEEDS to be fixed.
	 */
	private static int currentSoundID;
	private static int currentMusicID;
	
	/**
	 * 
	 */
	public SoundManager(Settings options)
	{
		settings = options;
	}
	
	public void init()
	{
		try
		{
			//Initialize OpenAL
			AL.create(null, 44100, 15, false);
			
			//Print openal version etc...
			if (Debug.checkLevel(Area.Sound, 1)) printSoundInfo();
		
			currentSoundID = 1;
			currentMusicID = 1;
		
			//Startup Player threads
			soundThread = new SoundThread(settings.maxSounds);
			musicThread = new MusicThread(settings.maxMusic);
			soundThread.start();
			musicThread.start();
		}
		catch (LWJGLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	private void printSoundInfo()
	{
		StringBuilder sb = new StringBuilder();
		
		IntBuffer buf = BufferUtils.createIntBuffer(2);
		ALC10.alcGetInteger(AL.getDevice(), ALC10.ALC_MAJOR_VERSION, buf);
		buf.position(1);
		ALC10.alcGetInteger(AL.getDevice(), ALC10.ALC_MINOR_VERSION, buf);
		
		sb.append("OpenAL version: ");
		sb.append(buf.get(0));
		sb.append(".");
		sb.append(buf.get(1));
		
		String device = ALC10.alcGetString(AL.getDevice(), ALC10.ALC_DEVICE_SPECIFIER);
		device = device.substring(0, device.length() - 2);
		sb.append("  DEVICE: ");
		sb.append(device);
		
		Debug.out(Area.Sound, 5, sb.toString());
	}
	
	/**
	 * Plays given soundID (if a sound slot is available)
	 * @param soundID
	 */
	public static int playSound(int soundID)
	{
		//Get next sound identifier
		int id = currentSoundID;
		currentSoundID++;
		if (currentSoundID > 0xFF) currentSoundID = 1;
		
		//Issue play
		Debug.out(Area.Sound, 6, "Playing Sound: " + soundID);
		soundThread.doCommand(id, SoundThread.CMD_PLAY, soundID);
		
		return id;
	}
	
	public static void stopSound(int id)
	{
		//Issue stop
		Debug.out(Area.Sound, 6, "Stopping Sound with id: " + id);
		soundThread.doCommand(0, SoundThread.CMD_STOP, id);
	}
	
	public static void stopAllSound()
	{
		//Issue stop all
		Debug.out(Area.Sound, 6, "Stopping all Sound");
		soundThread.doCommand(0, SoundThread.CMD_STOP_ALL, 0);
	}
		
	/**
	 * Plays given musicID in given slot.<BR>
	 * It replaces any currently running music in given slot.
	 * @param musicSlot
	 * @param musicID
	 */
	public static int playMusic(int musicID, boolean loop)
	{
		//Get next sound identifier
		int id = currentMusicID;
		currentMusicID++;
		if (currentMusicID > 0xFF) currentMusicID = 1;
		
		//Issue play
		Debug.out(Area.Sound, 6, "Playing Music: " + musicID);
		musicThread.doCommand(id, MusicThread.CMD_PLAY, musicID);
		musicThread.doCommand(id, MusicThread.CMD_LOOP, loop ? MusicThread.TRUE :  MusicThread.FALSE);
		
		return id;
	}
	
	public static void stopMusic(int id)
	{
		//Issue stop
		Debug.out(Area.Sound, 6, "Stopping Music with id: " + id);
		musicThread.doCommand(0, MusicThread.CMD_STOP, id);
	}
	
	public void dispose()
	{
		soundThread.doCommand(0, SoundThread.CMD_TERMINATE, 0);
		musicThread.doCommand(0, MusicThread.CMD_TERMINATE, 0);
		
		try
		{
			soundThread.join();
			musicThread.join();
		}
		catch (InterruptedException e)
		{
			
		}
		
		
		//TODO dispose off sound resources (buffers and sources)
		
		AL.destroy();
	}
}
