/**
 * RGBAImage.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

/**
 * @author Heiko Schmitt
 *
 */
public class RGBAImage
{
	private int width;
	private int height;
	private int scanlineSize;
	
	private byte[] data;
	
	/**
	 * Contains the imagedata in RGBA fashion
	 */
	public RGBAImage(int width, int height, int[] pixelData)
	{
		this.width = width;
		this.height = height;
		
		init();
		copyFromIntData(pixelData);
	}

	private void init()
	{
		data = new byte[width * height * 4];
		scanlineSize = width * 4;
	}
	
	private void copyFromIntData(int[] pixelData)
	{
		int xoff;
		int yoff = 0;
		for (int y = 0; y < height; y++)
		{
			xoff = 0;
			for (int x = 0; x < width; x++)
			{
				data[xoff + 0 + yoff] = (byte)((pixelData[x + (y * width)] >> 16) & 0xFF);
				data[xoff + 1 + yoff] = (byte)((pixelData[x + (y * width)] >>  8) & 0xFF);
				data[xoff + 2 + yoff] = (byte)((pixelData[x + (y * width)] >>  0) & 0xFF);
				data[xoff + 3 + yoff] = (byte)((pixelData[x + (y * width)] >> 24) & 0xFF);
				
				xoff += 4;
			}
			yoff += scanlineSize;
		}
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public byte[] getData()
	{
		return data;
	}
	
	public int getSize()
	{
		return width * height * 4;
	}
}
