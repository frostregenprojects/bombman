/**
 * TextureManager.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import bombman.application.Settings;
import bombman.debug.Debug;
import bombman.debug.Debug.Area;

/**
 * @author Heiko Schmitt
 *
 */
public class TextureManager
{	
	/*
	 * NOTE:
	 * There are multiple identifiers:
	 * TexID references an actual image. (xyz.png)
	 * TextureID references a part of an image. (0 0 32 32 of xyz.png) 
	 */
	
	/**
	 * Contains TextureID -> Texture Mapping
	 */
	private Texture[] textures;
	
	/**
	 * Contains TexID -> TextureFile Mapping
	 */
	private TextureFile[] textureFiles;
	
	/**
	 * Temporary upload buffer RAM -> VRAM
	 */
	private ByteBuffer dataBuffer;
	
	/**
	 * Maximum texture size. (used by temporary upload buffer)
	 */
	private static int maxTextureSize = 1024;
	
	private Settings settings;
	private static final String baseTexturePath = "res/texture/";
	private int boundTexID;
	
	//Threaded loading:
	private Queue<Integer> loadQueue;
	private Queue<Integer> readyQueue;
	private ImageLoadingThread loadThread;
	
	/**
	 * Creates a new TextureManager<BR>
	 * Texture definitions are loaded from given textureFile<BR>
	 * Must be disposed! (texMan.dispose())
	 * @param textureFile
	 */
	public TextureManager(File textureFile, Settings settings)
	{
		this.settings = settings;
		
		dataBuffer = BufferUtils.createByteBuffer(maxTextureSize * maxTextureSize * 4);
		loadQueue = new ConcurrentLinkedQueue<Integer>();
		readyQueue = new ConcurrentLinkedQueue<Integer>();
		
		readTextureFile(textureFile);
		
		loadThread = new ImageLoadingThread(loadQueue, readyQueue, baseTexturePath, textureFiles);
		loadThread.start();
	}
	
	/**
	 * Unsets the currently bound texture
	 */
	public void resetBoundTexture()
	{
		boundTexID = -1;
	}
	
	private void processReadyQueue()
	{
		int readyID;
		while (!readyQueue.isEmpty())
		{
			readyID = readyQueue.poll();
			textureFiles[readyID].textureDataLoaded = true;
			textureFiles[readyID].queuedForLoading = false;
		}
	}
	
	public void preloadTextures()
	{
		for (int i = 0; i < textureFiles.length; i++)
		{
			if (textureFiles[i] != null)
				loadTextureByID(i, false);
		}
		//TODO: WAIT here till all textures are loaded!!!! (fixes white FBO bug)
		//Simple wait/notify? or sleep may help...
		//Or kill thread and join here
		//Or much easier: call the loading routine directly (supply param: loadThreaded)
		//Somehow check if non-square textures(FBO?) are supported
	}
	
	/**
	 * Call this at the beginning of each frame
	 */
	public void prepareNewFrame()
	{
		resetBoundTexture();
		processReadyQueue();
	}
	
	private void readTextureFile(File textureFile)
	{
		try
		{
			LinkedHashMap<String, Integer> texFileMap = new LinkedHashMap<String, Integer>();
			
			//Read in TextureFiles
			int currentID = 0;
			int texCount = 0;
			BufferedReader br = new BufferedReader(new FileReader(textureFile));
			String line = br.readLine();
			String[] splittedLine;
			while (line != null)
			{
				splittedLine = line.split("\t");
				if (splittedLine.length == 6)
				{
					if (!texFileMap.containsKey(splittedLine[5]))
					{
						texFileMap.put(splittedLine[5], currentID);
						currentID++;
					}
					if (Integer.parseInt(splittedLine[0]) >= texCount)
						texCount = Integer.parseInt(splittedLine[0]) + 1;
				}
				line = br.readLine();
			}
			br.close();
			
			//copy to mapping array
			textures = new Texture[texCount];
			textureFiles = new TextureFile[currentID];
			Iterator<String> it = texFileMap.keySet().iterator();
			while (it.hasNext())
			{
				String s = it.next();
				textureFiles[texFileMap.get(s)] = new TextureFile(s);
			}
			
			//Read in Textures
			br = new BufferedReader(new FileReader(textureFile));
			line = br.readLine();
			Texture newTex;
			int texID;
			int currentLine = 1;
			while (line != null)
			{
				splittedLine = line.split("\t");
				if (splittedLine.length == 6)
				{
					newTex = new Texture(Integer.parseInt(splittedLine[1]), Integer.parseInt(splittedLine[2]), Integer.parseInt(splittedLine[3]), Integer.parseInt(splittedLine[4]), texFileMap.get(splittedLine[5]));
					texID = Integer.parseInt(splittedLine[0]);
					if (textures[texID] == null)
						textures[texID] = newTex;
					else
						Debug.out(Area.Render, 3, "texTable: ID: " + texID + " is already used! ignoring line: " + currentLine);
				}
				line = br.readLine();
				currentLine++;
			}
			br.close();
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void loadTextureByID(int texID, boolean loadThreaded)
	{
		if (!textureFiles[texID].queuedForLoading)
		{
			if (loadThreaded)
			{
				Debug.out(Area.Render, 5, "loading Texture: " + texID);
				textureFiles[texID].queuedForLoading = true;
				loadQueue.offer(texID);
				synchronized(loadThread)
				{
					loadThread.notify();
				}
			}
			else
			{
				loadThread.loadData(texID);
				textureFiles[texID].textureDataLoaded = true;
			}
		}
	}
	
	/**
	 * Binds and returns given texture
	 * @param textureID
	 * @param gl
	 * @return
	 */
	public Texture bindTextureByID(int textureID)
	{
		Debug.out(Area.Render, 10, "binding texture: " + textureID);
		
		Texture result = textures[textureID];
		TextureFile texFile = textureFiles[result.texID]; 
		if (!texFile.openGLloaded)
		{
			//Not inside VRAM. Check if in RAM.
			if (texFile.textureDataLoaded)
			{
				//Already in RAM, now copy to VRAM
				
				//Copy into uploadBuffer
				dataBuffer.clear();
				dataBuffer.put(texFile.textureData.getData(), 0, texFile.textureData.getSize());
				dataBuffer.flip();
				
				//Upload to GFXCard
				IntBuffer tmp = BufferUtils.createIntBuffer(1);
				GL11.glGenTextures(tmp);
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, tmp.get(0));
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
			    if (settings.filterTextures) //TextureFiltering
			    {
			    	GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
			    	GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
			    }
			    else
			    {
			    	GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
			    	GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
			    }
			    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, texFile.width, texFile.height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, dataBuffer);
				 
				//Cleanup
				texFile.releaseImageData();
				
				//Finished
				texFile.openGLtexID = tmp.get(0);
				texFile.openGLloaded = true;
			}
			else
			{
				loadTextureByID(result.texID, true);
			}
		}
		if (texFile.openGLloaded)
		{
			//Calculate TexCoords if necessary
			if (!result.texCoordsCalced) result.calculateTexCoords(texFile.width, texFile.height);
			
			//It is already loaded into VRAM. Just bind it, if not already
			if (texFile.openGLtexID != boundTexID)
			{
				boundTexID = texFile.openGLtexID;
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, boundTexID);
			}
		}
		else
		{
			//It is not loaded, bind default texture
			boundTexID = 0;
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, boundTexID);
		}
		
		return result;
	}
	
	public void dispose()
	{
		loadThread.terminate();
	}
}
