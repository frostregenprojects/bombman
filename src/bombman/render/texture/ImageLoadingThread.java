/**
 * ImageLoadingThread.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

import java.io.File;
import java.util.Queue;

/**
 * @author Heiko Schmitt
 *
 */
public class ImageLoadingThread extends Thread
{
	private String baseTexturePath;
	private TextureFile[] textureFiles;
	private Queue<Integer> loadQueue;
	private Queue<Integer> readyQueue;
	
	private boolean keepRunning;
		
	/**
	 * 
	 */
	public ImageLoadingThread(Queue<Integer> loadQueue, Queue<Integer> readyQueue, String baseTexturePath, TextureFile[] textureFiles)
	{
		this.keepRunning = true;
		this.loadQueue = loadQueue;
		this.readyQueue = readyQueue;
		this.baseTexturePath = baseTexturePath;
		this.textureFiles = textureFiles;
	}

	public void loadData(int textureFileID)
	{
		TextureFile loadTex = textureFiles[textureFileID];
		
		loadTex.textureData = ImageLoader.loadFromFile(new File(baseTexturePath + loadTex.filename));
		loadTex.width = loadTex.textureData.getWidth();
		loadTex.height = loadTex.textureData.getHeight();
	}
	
	@Override
	public void run()
	{
		int nextToProcess;
		while (keepRunning)
		{
			if (!loadQueue.isEmpty())
			{
				nextToProcess = loadQueue.poll();
				loadData(nextToProcess);
				readyQueue.offer(nextToProcess);
			}
			else
			{
				try
				{
					synchronized(this)
					{
						wait();
					}
				}
				catch (InterruptedException e)
				{
				}
			}
		}
	}
	
	public synchronized void terminate()
	{
		keepRunning = false;
		notify();
	}
}
