/**
 * ImageLoader.java 
 * Created on 17.10.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.File;

/**
 * @author Heiko Schmitt
 *
 */
public class ImageLoader
{
	private ImageLoader()
	{
		// Hidden constructor
	}

	public static RGBAImage loadFromFile(File imageFile)
	{
		RGBAImage result = null;
		try
		{
			Image image = Toolkit.getDefaultToolkit().getImage(imageFile.getAbsolutePath());
			while (image.getWidth(null) < 0) //Wait till image is loaded
			{
				try
				{
					Thread.sleep(1);
				}
				catch (InterruptedException e) 
				{
				}
			}
			
			int width = image.getWidth(null);
			int height = image.getHeight(null);
			
			int[] pixels = new int[width * height];
			PixelGrabber pg = new PixelGrabber(image, 0, 0, width, height, pixels, 0, width);
			pg.grabPixels();
			
			result = new RGBAImage(width, height, pixels);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return result;
	}
}
