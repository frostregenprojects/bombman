/**
 * TextureFile.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

/**
 * @author Heiko Schmitt
 *
 */
public class TextureFile
{
	public RGBAImage textureData;
	public boolean queuedForLoading;
	public boolean textureDataLoaded;
	
	public boolean openGLloaded;
	public int openGLtexID;
	
	public String filename;
	
	public int width;
	public int height;
	
	public TextureFile(String filename)
	{
		this.filename = filename;
		this.textureData = null;
		this.textureDataLoaded = false;
		this.queuedForLoading = false;
		this.openGLloaded = false;
		this.openGLtexID = 0;
	}

	public void releaseImageData()
	{
		textureDataLoaded = false;
		textureData = null;
	}
}
