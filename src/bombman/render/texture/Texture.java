/**
 * Texture.java 
 * Created on 14.11.2008
 * by Heiko Schmitt
 */
package bombman.render.texture;

/**
 * @author Heiko Schmitt
 *
 */
public class Texture
{
	public int x;
	public int y;
	public int width;
	public int height;
	public int texID;
	
	public boolean texCoordsCalced;
	public float[] texCoord1;
	public float[] texCoord2;
	public float[] texCoord3;
	public float[] texCoord4;
	
	public Texture(int x, int y, int width, int height, int texID)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.texID = texID;
		
		this.texCoordsCalced = false;
		texCoord1 = new float[] { 0, 0 };
		texCoord2 = new float[] { 0, 1 };
		texCoord3 = new float[] { 1, 1 };
		texCoord4 = new float[] { 1, 0 };
	}
	
	public void calculateTexCoords(float texWidth, float texHeight)
	{
		float onePixelHoriz = (1 / texWidth);
		float onePixelVert = (1 / texHeight);
		
		texCoord1[0] = (x / texWidth) + onePixelHoriz;				texCoord1[1] = (y / texHeight) + onePixelVert;
		texCoord2[0] = (x / texWidth) + onePixelHoriz;				texCoord2[1] = ((y + height) / texHeight) - onePixelVert;
		texCoord3[0] = ((x + width) / texWidth) - onePixelHoriz;	texCoord3[1] = ((y + height) / texHeight) - onePixelVert;
		texCoord4[0] = ((x + width) / texWidth) - onePixelHoriz;	texCoord4[1] = (y / texHeight) + onePixelVert;
		texCoordsCalced = true;
	}
}
