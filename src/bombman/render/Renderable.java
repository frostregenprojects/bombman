/**
 * Renderable.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.render;

/**
 * @author Heiko Schmitt
 *
 */
public interface Renderable
{
	public void render(float advance);
	
	public boolean emitsLight();
	public void renderLight(float advance);
}
