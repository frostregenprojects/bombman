/**
 * Renderer.java 
 * Created on 08.05.2009
 * by Heiko Schmitt
 */
package bombman.render;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;

import bombman.application.Settings;
import bombman.debug.Debug;
import bombman.debug.Debug.Area;
import bombman.game.FastMath;
import bombman.render.shader.ShaderManager;
import bombman.render.texture.Texture;
import bombman.render.texture.TextureManager;

/**
 * @author Heiko Schmitt
 *
 */
public class Renderer
{
	private static Settings settings;
	private static TextureManager texMan;
	private static ShaderManager shaderMan;
	private static int objectCountLastFrame;
	
	public static final int TILESIZE = 32;
	public static final int TILESIZE_H = TILESIZE / 2;
	
	//FBO and staticTexture
	private static int frameBufferObjectID;
	public static int backgroundTextureID;
	public static int normalTextureID;
	public static int fullscreenBufferTextureID;
	
	private static float fullscreenTextureCoordX;
	private static float fullscreenTextureCoordY;
	
	/**
	 * 
	 */
	public Renderer(Settings options)
	{
		settings = options;
		texMan = new TextureManager(new File("res/texture/texturetable.ini"), settings);
		shaderMan = new ShaderManager();
		backgroundTextureID =  -1;
		normalTextureID = -1;
		fullscreenBufferTextureID = -1;
	}

	public void init()
	{
		//Create Display
		try
		{
			setDisplayMode(settings.displayWidth, settings.displayHeight, settings.displayBpp, settings.displayHz);
			Display.setTitle("Bombman");
			Display.setFullscreen(settings.fullscreen);
			Display.create();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		if (!checkHardware())
		{
			//Dialog d = new Dialog()
			System.out.println("There are missing extensions... aborting.");
			System.exit(1);
		}
		
		//Init Color
		GL11.glColor3f(1f, 1f, 1f);
		GL11.glClearColor(0, 0, 0, 1);
		
	    /* Activate Backface culling */
	    GL11.glEnable(GL11.GL_CULL_FACE);
	    GL11.glFrontFace(GL11.GL_CCW);
	    GL11.glCullFace(GL11.GL_BACK);
	    
		/* Set vsync */
	    Display.setVSyncEnabled(settings.vSync);
		
	    /* Disable depth test */
	    GL11.glDisable(GL11.GL_DEPTH_TEST);
	    
	    /* Disable lighting */
	    GL11.glDisable(GL11.GL_LIGHTING);
	    
	    /* Blending mode */
	    GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	    
	    /* Viewport */
	    GL11.glViewport(0, 0, settings.displayWidth, settings.displayHeight);
	    
	    /* Setup Offscreen buffers */
	    setupOffscreenBuffers();
	    
	    /* Preload textures */
	    texMan.preloadTextures();
	}
	
	public void dispose()
	{
		texMan.dispose();
		Display.destroy();
	}
	
	public void frameStart()
	{
		objectCountLastFrame = 0;
		texMan.prepareNewFrame();
	}

	public void frameEnd()
	{
		Display.sync(settings.gameFPS);
		Display.update();
	}
	
	public static void setupProjection()
	{
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluOrtho2D(0, settings.displayWidth, settings.displayHeight, 0);
		
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
	}
	
	/**
	 * Call everytime you manually change the bound texture
	 */
	public static void resetBoundTexture()
	{
		texMan.resetBoundTexture();
	}
	
	private void setupOffscreenBuffers()
	{
		if (settings.debugAlwaysRenderStatic) return; //We do not need Offscreen Buffers in this case
		
		//Check non-power-of-two support and set fbo dimensions accordingly:
		int fboWidth = settings.displayWidth;
		int fboHeight = settings.displayHeight;
		if (!settings.nonPowerOfTwoSupported)
		{
			if (fboWidth <= 0) fboWidth = FastMath.pow(2, (int)Math.ceil(Math.log10(settings.displayWidth) / Math.log10(2)));
			if (fboHeight <= 0) fboHeight = FastMath.pow(2, (int)Math.ceil(Math.log10(settings.displayHeight) / Math.log10(2)));
		}
		//Debug info:
		Debug.out(Area.Render, 1, "NonPowerOfTwo-Textures supported: " + settings.nonPowerOfTwoSupported);
		Debug.out(Area.Render, 1, "Background FBO size: " + fboWidth + "x" + fboHeight);
		
		//Now calculate texture offsets (if fbo size differs from screenSize)
		fullscreenTextureCoordX = (float)settings.displayWidth / (float)fboWidth;
		fullscreenTextureCoordY = (float)settings.displayHeight / (float)fboHeight;
		
		//Temp buffer
		IntBuffer tmp = BufferUtils.createIntBuffer(1);
		
		//Delete old stuff
		if (backgroundTextureID >= 0)
		{
			//Delete staticTextures
			tmp.put(backgroundTextureID);
			GL11.glDeleteTextures(tmp);
			tmp.put(fullscreenBufferTextureID);
			GL11.glDeleteTextures(tmp);
			tmp.put(normalTextureID);
			GL11.glDeleteTextures(tmp);
			
			//Delete Framebuffer
			tmp.put(frameBufferObjectID);
			EXTFramebufferObject.glDeleteFramebuffersEXT(tmp);
		}
		
		////////////////////////////////////////
		//Generate FBO
		////////////////////////////////////////
		EXTFramebufferObject.glGenFramebuffersEXT(tmp);
	    frameBufferObjectID = tmp.get(0);
	    EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, frameBufferObjectID);

	    //////////////////////////////
	    //Create backgroundTexture
	    //////////////////////////////
	    GL11.glGenTextures(tmp);
	    backgroundTextureID = tmp.get(0);
	    GL11.glBindTexture(GL11.GL_TEXTURE_2D, backgroundTextureID);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB8, fboWidth, fboHeight, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, (ByteBuffer)null);
	    
		//Attach texture to FBO
		EXTFramebufferObject.glFramebufferTexture2DEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT, GL11.GL_TEXTURE_2D, backgroundTextureID, 0);
		
		//Check FBO
		checkFBO();
		
		//Clear Texture
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		//////////////////////////////
		//Create fullscreenTexture
		//////////////////////////////
	    GL11.glGenTextures(tmp);
	    fullscreenBufferTextureID = tmp.get(0);
	    GL11.glBindTexture(GL11.GL_TEXTURE_2D, fullscreenBufferTextureID);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB8, fboWidth, fboHeight, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, (ByteBuffer)null);
	    
		//Attach texture to FBO
		EXTFramebufferObject.glFramebufferTexture2DEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT, GL11.GL_TEXTURE_2D, fullscreenBufferTextureID, 0);
		
		//Clear Texture
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		//////////////////////////////
		//Create normalTexture
		//////////////////////////////
	    GL11.glGenTextures(tmp);
	    normalTextureID = tmp.get(0);
	    GL11.glBindTexture(GL11.GL_TEXTURE_2D, normalTextureID);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
	    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, settings.filterTextures ? GL11.GL_LINEAR : GL11.GL_NEAREST);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB8, fboWidth, fboHeight, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, (ByteBuffer)null);
	    
		//Attach texture to FBO
		EXTFramebufferObject.glFramebufferTexture2DEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT, GL11.GL_TEXTURE_2D, normalTextureID, 0);
		
		//Clear Texture
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		
	    EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
	}
	
	public static void checkFBO()
	{
		int error = EXTFramebufferObject.glCheckFramebufferStatusEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT);
		switch (error)
		{
			case EXTFramebufferObject.GL_FRAMEBUFFER_COMPLETE_EXT: break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT: 
				Debug.out(Area.Render, 1, "Incomplete attachment"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
				Debug.out(Area.Render, 1, "Missing attachment"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
				Debug.out(Area.Render, 1, "Incomplete dimensions"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
				Debug.out(Area.Render, 1, "Incomplete formats"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
				Debug.out(Area.Render, 1, "Incomplete draw buffer"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
				Debug.out(Area.Render, 1, "Incomplete read buffer"); break;
			case EXTFramebufferObject.GL_FRAMEBUFFER_UNSUPPORTED_EXT:
				Debug.out(Area.Render, 1, "Framebufferobjects unsupported"); break;
			default:
				Debug.out(Area.Render, 1, "unknowkn FBO error: " + error); break;
		}
	}
	
	private void setDisplayMode(int width, int height, int bitsPerPixel, int hz) throws LWJGLException
	{
		DisplayMode mode = findDisplayMode(width, height, bitsPerPixel, hz);
		if (mode == null) mode = new DisplayMode(width, height);
		Display.setDisplayMode(mode);
	}
		
	private DisplayMode findDisplayMode(int width, int height, int bpp, int hz) throws LWJGLException
	{
		DisplayMode[] modes = Display.getAvailableDisplayModes();
		for (int i = 0; i < modes.length; i++)
		{
			if ((modes[i].getWidth() == width)
					&& (modes[i].getHeight() == height)
					&& (modes[i].getBitsPerPixel() == bpp)
					&& (modes[i].getFrequency() == hz))
			{
				return modes[i];
			}
		}
		return null;
	}
	
	public boolean checkHardware()
	{
		if (settings.debugAlwaysRenderStatic) return true; //This disables all OpenGL checks too
		boolean result = true;
		
		//Check openGL Version
		ContextCapabilities cc = GLContext.getCapabilities();
		String openGLVersion = GL11.glGetString(GL11.GL_VERSION);
	    if (!cc.OpenGL20)
	    {
	    	Debug.out(Area.Render, 1, "OpenGL version: " + openGLVersion + " -> (failed: < 2.0)");
	    	result = false;
	    }
	    else
	    	Debug.out(Area.Render, 1, "OpenGL version: " + openGLVersion + " -> (ok)");
	    	
	    //check needed Extensions
	    if (!cc.GL_EXT_framebuffer_object) { result = false; Debug.out(Area.Render, 1, "GL_EXT_framebuffer_object is missing."); }
	    settings.nonPowerOfTwoSupported = cc.GL_ARB_texture_non_power_of_two;
	    
		return result;
	}
	
	////////////////////////////////////////////////
	//RENDERING UTILITY METHODS
	////////////////////////////////////////////////
	
	//FBO methods
	public static void enableFBO(int textureTarget)
	{
		EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, frameBufferObjectID);
		EXTFramebufferObject.glFramebufferTexture2DEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, EXTFramebufferObject.GL_COLOR_ATTACHMENT0_EXT, GL11.GL_TEXTURE_2D, textureTarget, 0);
	}
	
	public static void disableFBO()
	{
		EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
	}
	
	//Shader methods
	public static void bindShader(String shaderName)
	{
		shaderMan.bindShader(shaderName);
	}
	
	public static void unbindShader()
	{
		shaderMan.unbindShader();
	}
	
	public static int getUniformLocation(String uniformName)
	{
		return shaderMan.getUniformLocation(uniformName);
	}
	
	/**
	 * For use without texture manager
	 * @param tex set to -1 for no texture change
	 */
	public static void drawFullscreenTexture(int tex)
	{
		//Bind Texture
		if (tex >= 0)
		{
			texMan.resetBoundTexture();
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex);
		}
		
		//Draw Quad
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0, fullscreenTextureCoordY);
			GL11.glVertex2f(0, 0);
				
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2f(0, settings.displayHeight);
				
			GL11.glTexCoord2f(fullscreenTextureCoordX, 0);
			GL11.glVertex2f(settings.displayWidth, settings.displayHeight);
				
			GL11.glTexCoord2f(fullscreenTextureCoordX, fullscreenTextureCoordY);
			GL11.glVertex2f(settings.displayWidth, 0);
		GL11.glEnd();
	}
	
	public static void drawImage(float w, float h, int imageID)
	{
		//Bind image
		Texture tex = texMan.bindTextureByID(imageID);
		
		//Calc halfsizes
		float left = -w / 2;
		float right = w / 2;
		float up = -h / 2;
		float down = h / 2;
		
		//Draw Quad
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(tex.texCoord1[0], tex.texCoord1[1]);
			GL11.glVertex2f(left, up);
				
			GL11.glTexCoord2f(tex.texCoord2[0], tex.texCoord2[1]);
			GL11.glVertex2f(left, down);
				
			GL11.glTexCoord2f(tex.texCoord3[0], tex.texCoord3[1]);
			GL11.glVertex2f(right, down);
				
			GL11.glTexCoord2f(tex.texCoord4[0], tex.texCoord4[1]);
			GL11.glVertex2f(right, up);
		GL11.glEnd();
		
		objectCountLastFrame++;
	}
	
	public static void draw(int textureID, float width, float height, float x, float y, int zLevel, float rotZ, boolean keepMatrix)
	{
		if (keepMatrix) GL11.glPushMatrix();
		
		//Translation
		GL11.glTranslatef(x, y, zLevel);
		
		//Rotation
		if (rotZ != 0) GL11.glRotatef(rotZ, 0, 0, 1);
				
		//Drawing
		drawImage(width, height, textureID);
		
		if (keepMatrix) GL11.glPopMatrix();
	}
	
	public static void drawString(String text, int size, float x, float y, int zLevel, float rotZ, float r, float g, float b, boolean keepMatrix)
	{
		if (keepMatrix) GL11.glPushMatrix();
		
		//Color
		GL11.glColor3f(r, g, b);
		
		//Translation
		GL11.glTranslatef(x - (size >> 2), y, zLevel);
		
		//Rotation
		if (rotZ != 0) GL11.glRotatef(rotZ, 0, 0, 1);
		
		GL11.glPushMatrix();
		int len = text.length();
		for (int i = 0; i < len; i++)
		{
			//Translation
			GL11.glTranslatef(size, 0, 0);
				
			//Drawing
			drawImage(size, size, text.charAt(i));
		}
		GL11.glPopMatrix();
		
		//Reset Color
		GL11.glColor3f(1, 1, 1);
		
		if (keepMatrix) GL11.glPopMatrix();
	}
}
