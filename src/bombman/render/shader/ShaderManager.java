/**
 * ShaderManager.java 
 * Created on 09.07.2008
 * by Heiko Schmitt
 */
package bombman.render.shader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL20;

/**
 * @author Heiko Schmitt
 *
 */
public class ShaderManager
{
	private static final String shaderPath = "res/shader/";
	private LinkedHashMap<String, Integer> programTable;
	private LinkedHashMap<String, Integer> uniformLocationTable;
	private int currentProgramID;
	private String currentProgramName;
	private Charset charset = Charset.forName("ASCII");
	
	public ShaderManager()
	{
		this.programTable = new LinkedHashMap<String, Integer>();
		this.uniformLocationTable = new LinkedHashMap<String, Integer>(); 
		this.currentProgramID = 0;
	}

	public void unbindShader()
	{
		currentProgramName = "";
		currentProgramID = 0;
		GL20.glUseProgram(currentProgramID);
	}
	
	private void loadShader(String shaderName)
	{
		ByteBuffer buf = null;
		// Load vertexShader
		try
		{
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(new File(shaderPath + shaderName + ".vs")));
			String line = reader.readLine();
			while (line != null)
			{
				sb.append(line + "\n");
				line = reader.readLine();
			}
			byte[] data = sb.toString().getBytes();
			buf = BufferUtils.createByteBuffer(data.length);
			buf.put(data);
			buf.flip();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		// Create VertexShader
		int vertexShaderID = GL20.glCreateShader(GL20.GL_VERTEX_SHADER);
		GL20.glShaderSource(vertexShaderID, buf);
		GL20.glCompileShader(vertexShaderID);
		glPrintInfoLog(shaderName + "\nVertex: ", vertexShaderID);
		
		// Load fragmentShader
		try
		{
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(new File(shaderPath + shaderName + ".fs")));
			String line = reader.readLine();
			while (line != null)
			{
				sb.append(line + "\n");
				line = reader.readLine();
			}
			byte[] data = sb.toString().getBytes();
			buf = BufferUtils.createByteBuffer(data.length);
			buf.put(data);
			buf.flip();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	    
	    // Create FragmentShader
	    int fragmentShaderID = GL20.glCreateShader(GL20.GL_FRAGMENT_SHADER);
	    GL20.glShaderSource(fragmentShaderID, buf);
	    GL20.glCompileShader(fragmentShaderID);
	    glPrintInfoLog(shaderName + "\nFragment: ", fragmentShaderID);
	    
	    // Create ShaderProgram
	    int shaderProgramID = GL20.glCreateProgram();
	    GL20.glAttachShader(shaderProgramID, vertexShaderID);
	    GL20.glAttachShader(shaderProgramID, fragmentShaderID);
	    GL20.glLinkProgram(shaderProgramID);
	    glPrintInfoLog(shaderName + "\nLink: ", shaderProgramID);
	    GL20.glValidateProgram(shaderProgramID);

	    // Insert ShaderProgram into table
	    String key = shaderName;
	    programTable.put(key, shaderProgramID);
	}
	
	private void glPrintInfoLog(String prefix, int objectID)
	{
		IntBuffer tmp = BufferUtils.createIntBuffer(1);
		ARBShaderObjects.glGetObjectParameterARB(objectID, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB, tmp);
		if (tmp.get(0) > 1)
		{
			ByteBuffer buf = BufferUtils.createByteBuffer(tmp.get(0));
			ARBShaderObjects.glGetInfoLogARB(objectID, tmp, buf);
			
			// Convert to byte array
			byte[] arr = new byte[tmp.get(0)];
			buf.get(arr);
			
			System.out.println(prefix);
			System.out.println(new String(arr, charset));
		}
	}
	
	public void bindShader(String shaderName)
	{
		currentProgramName = shaderName;
		if (!programTable.containsKey(currentProgramName))
			loadShader(currentProgramName);
		currentProgramID = programTable.get(currentProgramName); 
		GL20.glUseProgram(currentProgramID);
	}
	
	public int getCurrentProgramID()
	{
		return currentProgramID;
	}
	
	/**
	 * Operates on currently bound shader
	 * @return
	 */
	public int getUniformLocation(String uniformName)
	{
		StringBuilder sb = new StringBuilder(currentProgramName.length() + 1 + uniformName.length());
		sb.append(currentProgramName);
		sb.append("_");
		sb.append(uniformName);
		String key = sb.toString();
		
		if (!uniformLocationTable.containsKey(key))
		{
			byte[] data = uniformName.getBytes();
			ByteBuffer buf = BufferUtils.createByteBuffer(data.length + 1);
			buf.put(data);
			buf.put((byte) 0); // Wants null terminated string
			buf.flip();
			uniformLocationTable.put(key, GL20.glGetUniformLocation(currentProgramID, buf));
		}
		
		return uniformLocationTable.get(key);
	}
	
}
