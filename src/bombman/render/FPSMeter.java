/**
 * FPSMeter.java 
 * Created on 01.10.2007
 * by Heiko Schmitt
 */
package bombman.render;

/**
 * @author Heiko Schmitt
 *
 */
public class FPSMeter
{	
	private long lastSecond;
	private int fps;
	private int fpsLast;
	
	/**
	 * Hide...
	 */
	public FPSMeter() 
	{
		lastSecond = System.nanoTime();
		fps = 0;
		fpsLast = 0;
	}
	
	public void tick()
	{
		long now = System.nanoTime();
		if (now >= (lastSecond + 1000000000))
	    {
			lastSecond = lastSecond + 1000000000;
	        fpsLast = fps;
	        fps = 1;
	    } 
		else
	    {
	    	fps++;
	    }
	}
	
	/**
	 * @return the fps
	 */
	public int getFps()
	{
		return fpsLast;
	}
}
