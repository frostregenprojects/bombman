/**
 * Player.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import java.util.Iterator;

import bombman.input.KeyMapping.Commands;
import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class Player extends BaseObject implements Controllable
{
	private int animationFrame;
	private int animationBase;
	private int currentAnimBase;
	
	//Time dependent values
	private float baseSpeed;
	private float speedBonus;
	private float kickSpeed;
	private int animTickDelay;
	private int animModuloTest;
	private int bombTime;
	
	//Status
	private boolean isAlive;
	private int age;
	
	//Input Status
	private static final float WALK_RADIUS = 0.45f;
	private static final float CHECK_RADIUS = 0.01f;
	private boolean up, down, left, right;
	private boolean bombButtonPressed;
	private boolean specialButtonPressed;
	private int bombsAvailable;
	
	//Powerups
	private static final int MAX_BOMB_STRENGTH = 99999;
	private int maxBombs;
	private int powBombs;
	private int flames;
	private int goldenFlames;
	private int speeds;
	private int gloves;
	private int kicks;
	
	//Illness
	
	//Gameplay
	private int playerID;
	private String playerName;
	
	/**
	 * @param world
	 */
	public Player(World world, int playerID)
	{
		super(world);
		this.playerID = playerID;
		this.playerName = null;
		isPlayer = true;
		
		isAlive = true;
		age = 0;
		zLevel = 1;
		up = false;
		down = false;
		left = false;
		right = false;
		
		baseSpeed = 0.04f / speedFactor;
		speedBonus = baseSpeed / 3f;
		kickSpeed = speedBonus;
		
		//Powerups
		maxBombs = 1;
		powBombs = 0;
		flames = 0;
		speeds = 0;
		gloves = 0;
		kicks = 0;
		
		bombsAvailable = maxBombs;
		bombTime = FastMath.round(180 * speedFactor);
		
		animationBase = 544;
		//animationBase = 672;
		currentAnimBase = animationBase;
		animationFrame = 0;
		animTickDelay = Math.max(2, FastMath.round(6 * speedFactor));
		animModuloTest = animTickDelay - 1;
	}

	@Override
	public void tick(long frame)
	{
		if (isAlive)
		{
			//Input/Movement
			if (bombButtonPressed)
			{
				bombButtonPressed = false;
				//Check if Floor is clear:
				if (bombsAvailable > 0)
				{
					if (world.isTileClear(x, y))
					{
						Bomb newBomb = new Bomb(world, bombTime, (goldenFlames > 0) ? MAX_BOMB_STRENGTH : (flames + 2), this);
						world.addObject(newBomb, FastMath.round(x), FastMath.round(y), true);
						bombsAvailable--;
					}
				}
			}
			if (specialButtonPressed)
			{
				specialButtonPressed = false;
				//Do something
			}
			if (left || right || up || down)
			{
				float vx = 0;
				float vy = 0;
				if (left) vx -= getSpeed();
				if (right) vx += getSpeed();
				if (up) vy -= getSpeed();
				if (down) vy += getSpeed();
				
				//Walk Animation
				if ((frame % animTickDelay) == animModuloTest) animationFrame++;
				if (animationFrame >= 4) animationFrame = 0;
				
				MapTile tileSelf = world.getTile(x, y);
				MapTile tileNextCenter, tileNextWalk1, tileNextWalk2, tileNextCheck1, tileNextCheck2;
				if (right)
				{
					tileNextCenter = world.getTile(x + vx + WALK_RADIUS, y);
					tileNextWalk1 = world.getTile(x + vx + WALK_RADIUS, y - WALK_RADIUS);
					tileNextWalk2 = world.getTile(x + vx + WALK_RADIUS, y + WALK_RADIUS);
					tileNextCheck1 = world.getTile(x + vx + WALK_RADIUS, y - CHECK_RADIUS);
					tileNextCheck2 = world.getTile(x + vx + WALK_RADIUS, y + CHECK_RADIUS);
					if ((tileSelf == tileNextWalk1) && (tileSelf == tileNextWalk2))
					{
						if (tileSelf.isPassable || tileSelf.isOccupiedByBomb)
							x = x + vx;
					}
					else
					{
						if ((tileNextWalk1.isPassable) && (tileNextWalk2.isPassable)) //Both checkTiles are passable. normal move
							x = x + vx;
						else if ((tileNextWalk1.isPassable) && (tileNextCheck1.isPassable) && (vy == 0)) //Only one checktile is passable. try move towards passable
							y = y - Math.min(Math.abs(FastMath.round(y) - y), getSpeed());
						else if ((tileNextWalk2.isPassable) && (tileNextCheck2.isPassable) && (vy == 0))
							y = y + Math.min(Math.abs(FastMath.round(y) - y), getSpeed()); //Limit movement to tile center
						
						tryKick(tileNextCenter, (getSpeed() + kickSpeed), 0);
					}
					currentAnimBase = animationBase + 8;
				}
				if (left)
				{
					tileNextCenter = world.getTile(x + vx - WALK_RADIUS, y);
					tileNextWalk1 = world.getTile(x + vx - WALK_RADIUS, y - WALK_RADIUS);
					tileNextWalk2 = world.getTile(x + vx - WALK_RADIUS, y + WALK_RADIUS);
					tileNextCheck1 = world.getTile(x + vx - WALK_RADIUS, y - CHECK_RADIUS);
					tileNextCheck2 = world.getTile(x + vx - WALK_RADIUS, y + CHECK_RADIUS);
					if ((tileSelf == tileNextWalk1) && (tileSelf == tileNextWalk2))
					{
						if (tileSelf.isPassable || tileSelf.isOccupiedByBomb)
							x = x + vx;
					}
					else
					{
						if ((tileNextWalk1.isPassable) && (tileNextWalk2.isPassable))
							x = x + vx;
						else if ((tileNextWalk1.isPassable) && (tileNextCheck1.isPassable) && (vy == 0))
							y = y - Math.min(Math.abs(FastMath.round(y) - y), getSpeed());
						else if ((tileNextWalk2.isPassable) && (tileNextCheck2.isPassable) && (vy == 0))
							y = y + Math.min(Math.abs(FastMath.round(y) - y), getSpeed());
						
						tryKick(tileNextCenter, -(getSpeed() + kickSpeed), 0);
					}
					currentAnimBase = animationBase + 4;
				}
				if (down)
				{
					tileNextCenter = world.getTile(x, y + vy + WALK_RADIUS);
					tileNextWalk1 = world.getTile(x - WALK_RADIUS, y + vy + WALK_RADIUS);
					tileNextWalk2 = world.getTile(x + WALK_RADIUS, y + vy + WALK_RADIUS);
					tileNextCheck1 = world.getTile(x - CHECK_RADIUS, y + vy + WALK_RADIUS);
					tileNextCheck2 = world.getTile(x + CHECK_RADIUS, y + vy + WALK_RADIUS);
					if ((tileSelf == tileNextWalk1) && (tileSelf == tileNextWalk2))
					{
						if (tileSelf.isPassable || tileSelf.isOccupiedByBomb)
							y = y + vy;
					}
					else
					{
						if ((tileNextWalk1.isPassable) && (tileNextWalk2.isPassable))
							y = y + vy;
						else if ((tileNextWalk1.isPassable) && (tileNextCheck1.isPassable) && (vx == 0))
							x = x - Math.min(Math.abs(FastMath.round(x) - x), getSpeed());
						else if ((tileNextWalk2.isPassable) && (tileNextCheck2.isPassable) && (vx == 0))
							x = x + Math.min(Math.abs(FastMath.round(x) - x), getSpeed());
						
						tryKick(tileNextCenter, 0, (getSpeed() + kickSpeed));
					}
					currentAnimBase = animationBase;
				}
				if (up)
				{
					tileNextCenter = world.getTile(x, y + vy - WALK_RADIUS);
					tileNextWalk1 = world.getTile(x - WALK_RADIUS, y + vy - WALK_RADIUS);
					tileNextWalk2 = world.getTile(x + WALK_RADIUS, y + vy - WALK_RADIUS);
					tileNextCheck1 = world.getTile(x - CHECK_RADIUS, y + vy - WALK_RADIUS);
					tileNextCheck2 = world.getTile(x + CHECK_RADIUS, y + vy - WALK_RADIUS);
					if ((tileSelf == tileNextWalk1) && (tileSelf == tileNextWalk2))
					{
						if (tileSelf.isPassable || tileSelf.isOccupiedByBomb)
							y = y + vy;
					}
					else
					{
						if ((tileNextWalk1.isPassable) && (tileNextWalk2.isPassable))
							y = y + vy;
						else if ((tileNextWalk1.isPassable) && (tileNextCheck1.isPassable) && (vx == 0))
							x = x - Math.min(Math.abs(FastMath.round(x) - x), getSpeed());
						else if ((tileNextWalk2.isPassable) && (tileNextCheck2.isPassable) && (vx == 0))
							x = x + Math.min(Math.abs(FastMath.round(x) - x), getSpeed());
						
						tryKick(tileNextCenter, 0, -(getSpeed() + kickSpeed));
					}
					currentAnimBase = animationBase + 12;
				}
				if (tileSelf != world.getTile(x, y))
				{
					world.moveObject(this, tileSelf);
				}
			}
			//Check Objects on same tile
			MapTile tile = world.getTile(x, y);
			BaseObject obj;
			boolean isDeadly = tile.isDeadly;
			Iterator<BaseObject> it = tile.objects.values().iterator();
			while (it.hasNext() && (!isDeadly))
			{
				obj = it.next();
				
				//Check Deadly
				isDeadly = isDeadly | obj.isDeadly;
				
				//Check Items
				if (obj.isItem)
				{
					BonusItem item = (BonusItem) obj;
					//Remove item:
					world.removeObject(obj, true);
					
					//Add bonus effect
					switch (item.type)
					{
						case Speed: speeds++; break;
						case ExtraBomb: maxBombs++; bombsAvailable++; break;
						case FlameExtend: flames++; break;
						case GoldenFlameExtend: goldenFlames++; break;
						case PowerBomb: powBombs++; break;
						case Glove: gloves++; break;
						case Kick: kicks++; break;
							
						default: break;
					}
					
				}
			}
			if (isDeadly) die();
			if (isAlive) age++;
		}
		else
		{
			//Death animation
			if ((frame % animTickDelay) == animModuloTest) animationFrame++;
			if (animationFrame >= 4) world.removeObject(this, true);
		}
	}
	
	private void tryKick(MapTile tile, float vx, float vy)
	{
		if (kicks > 0)
		{
			if (tile.isOccupiedByBomb)
			{
				//Iterate through objects, check for bombs
				Iterator<BaseObject> it = tile.objects.values().iterator();
				BaseObject obj;
				Bomb bomb;
				while (it.hasNext())
				{
					obj = it.next();
					if (obj.isBomb)
					{
						bomb = (Bomb) obj;
						bomb.kick(vx, vy);
					}
				}
			}
		}
	}

	
	public void die()
	{
		if (isAlive)
		{
			isAlive = false;
			currentAnimBase = animationBase + 16;
			animationFrame = 0;
		}
	}
	
	@Override
	public void render(float advance)
	{
		Renderer.draw(currentAnimBase + animationFrame, 32, 48, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), (y * Renderer.TILESIZE), 0, 0, true);
		if (age < (300 / speedFactor))
		{
			if ((age % (int)(60 / speedFactor)) < (40 / speedFactor))
				Renderer.drawString((playerName == null) ? Integer.toString(playerID) : playerName, 16, (x * Renderer.TILESIZE) - Renderer.TILESIZE_H, (y * Renderer.TILESIZE) + Renderer.TILESIZE_H, 0, 0, 1, 0, 0, true);
		}
	}
	
	@Override
	public boolean hit()
	{
		die();
		return false;
	}

	private float getSpeed()
	{
		return baseSpeed + (speedBonus * speeds);
	}
	
	@Override
	public void processCommand(Commands command)
	{
		//System.out.println(command.name());
		switch (command)
		{
			case BOMB_BUTTON_PRESS: bombButtonPressed = true; break;
			case SPECIAL_BUTTON_PRESS: specialButtonPressed = true; break;
			
			case WALK_Y_DOWN: down = true; up = false; break;	//axis
			case WALK_Y_UP: up = true; down = false; break;		//axis
			case WALK_Y_STOP: down = false; up = false; break;	//axis
			case WALK_Y_DOWN_STOP: down = false; break;			//keyboard
			case WALK_Y_UP_STOP: up = false; break;				//keyboard
			
			case WALK_X_RIGHT: right = true; left = false; break;
			case WALK_X_LEFT: left = true; right = false; break;
			case WALK_X_STOP: right = false; left = false; break;
			case WALK_X_RIGHT_STOP: right = false; break;
			case WALK_X_LEFT_STOP: left = false; break;
			
			default: break;
		}
	}

	@Override
	public void notifyBombExploded()
	{
		bombsAvailable++;
	}
}
