/**
 * SuddenDeathBlock.java 
 * Created on 31.07.2011
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.game.MapTile.TileTypes;
import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class SuddenDeathBlock extends BaseObject
{
	private long lifeTime;
	private long maxTime;
	
	/**
	 * @param world
	 */
	public SuddenDeathBlock(World world)
	{
		super(world);
		affectsPassable = true;
		isDeadly = true;
		lifeTime = 0;
		maxTime = (secondsToTicks(1) / 2) * (secondsToTicks(1) / 2);
	}

	@Override
	public void tick(long frame)
	{
		lifeTime++;
		if ((lifeTime * lifeTime) > maxTime)
		{
			world.removeObject(this, true);
			world.getTile(x, y).setType(TileTypes.InnerWall);
		}
	}
	
	@Override
	public void render(float advance)
	{
		float factor = (((lifeTime * lifeTime) + advance) / (float)maxTime);
		if (factor > 1.0f)
		{
			factor = 1.0f;
		}
		factor = 2.0f - factor;
		// Draw shadow
		Renderer.draw(526, 32 / factor, 32 / factor, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
		// Draw falling down block
		Renderer.draw(514, 32 * factor, 32 * factor, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE) - ((factor - 1.0f) * 200), 0, 0, true);
	}
	
	@Override
	public boolean hit()
	{
		return true;
	}
}
