/**
 * Controllable.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.input.KeyMapping;

/**
 * @author Heiko Schmitt
 *
 */
public interface Controllable
{
	public void processCommand(KeyMapping.Commands command);
	public void notifyBombExploded();
}
