/**
 * UserInterface.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import org.lwjgl.opengl.GL11;

import bombman.application.Settings;
import bombman.menu.GameMenu;
import bombman.render.Renderer;
import bombman.sound.SoundManager;

/**
 * @author Heiko Schmitt
 *
 */
public class UserInterface extends BaseObject
{
	private Settings settings;
	private World gameWorld;
	private GameMenu gameMenu;
	
	public UserInterface(Settings options)
	{
		super(null);
		settings = options;
		
		//TODO this should happen later from GUI
		this.gameWorld = new World(settings);
		
		//Player test music
		if (settings.playMusic) SoundManager.playMusic(1, true);
		
		// Initialize game menu
		gameMenu = new GameMenu();
		gameMenu.setWorld(this.gameWorld);
	}

	@Override
	public void tick(long frame)
	{
		//Process gameworld
		if (gameWorld != null) gameWorld.tick(frame);
		
		//Process UI animation (and events!)
		if (gameMenu != null) gameMenu.tick(frame);
	}
	
	@Override
	public void render(float advance)
	{
		Renderer.setupProjection();
		
		//Clear screen
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		//Enable texturing
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glTexEnvi(GL11.GL_TEXTURE_ENV, GL11.GL_TEXTURE_ENV_MODE, GL11.GL_MODULATE);
		
		//First render gameWorld
		GL11.glPushMatrix();
			if (gameWorld != null) gameWorld.render(advance);
		GL11.glPopMatrix();
		
		//Then render ourself
		GL11.glPushMatrix();
			if (gameMenu != null) gameMenu.render(advance);
		GL11.glPopMatrix();
		//Renderer.drawString("Das kann Teil der GUI sein ;)", 16, 100, 200, 0, 0, 1, 0, 0, true);
	}
}
