/**
 * Explosion.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class Explosion extends BaseObject
{
	public ExplosionType type;
	public Controllable owner;
	private float rotZ;
	private int lightStrength;
	private int lightSprite;
	
	private int animationFrame;
	private int animationBase;
	private int animTickDelay;
	private int animModuloTest;
	private int lightAnim;
	private int animationSteps;
	
	private final int MAX_LIGHT_RADIUS = 20 * Renderer.TILESIZE;

	public enum ExplosionType
	{
		Horizontal,
		Vertical,
		Center,
		EndUp,
		EndDown,
		EndLeft,
		EndRight,
		End
	}
	
	/**
	 * @param world
	 */
	public Explosion(World world, ExplosionType type, Controllable owner)
	{
		super(world);
		this.owner = owner;
		isDeadly = true;
		lightStrength = 0;
		animationFrame = 0;
		animationSteps = 8;
		animTickDelay = Math.max(2, FastMath.round(6 * speedFactor));
		lightAnim = animationSteps * animTickDelay;
		animModuloTest = animTickDelay - 1;
		
		setType(type);
	}
	
	@Override
	public void tick(long frame)
	{
		//Animation
		if ((frame % animTickDelay) == animModuloTest)
			animationFrame++;
		if (animationFrame >= animationSteps)
			world.removeObject(this, true);
		if (lightAnim > 0) lightAnim--;
	}

	@Override
	public void render(float advance)
	{
		Renderer.draw(animationBase + animationFrame, 32, 32, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, rotZ, true);
	}
	
	public void setLightStrength(int lightStrength)
	{
		this.lightStrength = lightStrength;
	}
	
	public void setType(ExplosionType type)
	{
		this.type = type;
		lightSprite = 656;
		animationBase = 8;
		animationFrame = 0;
		rotZ = 0;
		
		switch (type)
		{
			case Vertical:
				rotZ = -90;
				break;
			case Center:
				animationBase = 24;
				break;
			case EndLeft:
				animationBase = 16;
				break;
			case EndRight:
				animationBase = 16;
				rotZ = 180;
				break;
			case EndUp:
				animationBase = 16;
				rotZ = 90;
				break;
			case EndDown:
				animationBase = 16;
				rotZ = -90;
				break;

			default:
				break;
		}
	}
	
	@Override
	public boolean emitsLight()
	{
		return (lightStrength > 0);
	}
	
	@Override
	public void renderLight(float advance)
	{
//		GL11.glColor4f(1, 1, 1, (float)Math.sin(Math.PI * ((float)lightAnim / (animationSteps * animTickDelay)) / 2));
//		Renderer.draw(lightSprite, lightStrength * Renderer.TILESIZE, lightStrength * Renderer.TILESIZE, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
		
		float size = Math.min(MAX_LIGHT_RADIUS, lightStrength * Renderer.TILESIZE * (float)Math.sin(Math.PI * ((float)lightAnim / (animationSteps * animTickDelay)) / 2));
		Renderer.draw(lightSprite, size, size, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
	}
}
