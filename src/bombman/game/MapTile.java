/**
 * MapTile.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * @author Heiko Schmitt
 *
 */
public class MapTile extends BaseObject
{
	public int textureID;
	public int normalTexID;
	public TileTypes type;
	
	public BonusItem item;
	public boolean isPassable;
	public boolean isOccupiedByBomb;
	public boolean isOccupiedByPlayer;
	
	public LinkedHashMap<Integer, BaseObject> objects;
	
	public enum TileTypes
	{
		Floor,
		Block,
		InnerWall,
		OuterWall,
		End
	}
	
	/**
	 * @param world
	 */
	public MapTile(World world, TileTypes type)
	{
		super(world);
		objects = new LinkedHashMap<Integer, BaseObject>();
		isOccupiedByBomb = false;
		isOccupiedByPlayer = false;
		
		setType(type);
	}

	@Override
	public void render(float advance)
	{
		//NOT USED... directly rendered in world...
		//Renderer.draw(textureID, 32, 32, x, y, 0, 0, true);
		//If above tile is a wall, render shadow
		//Renderer.draw(524, 32, 32, x, y, 0, 0, true);
	}
	
	@Override
	public void tick(long frame)
	{
		//NOT USED...
	}

	public void setBonusItem(BonusItem item)
	{
		this.item = item;
	}
	
	public void setType(TileTypes type)
	{
		this.type = type;
		textureID = world.tileSet[type.ordinal()];
		normalTexID = world.tileSetNormal[type.ordinal()];
		
		//Set properties
		if (type == TileTypes.Floor)
			isPassable = true;
		else
			isPassable = false;
		
		//This tile needs rerender
		world.drawToStatic(this);
	}
	
	public void addObject(BaseObject object)
	{
		objects.put(object.id, object);
	}
	
	public boolean removeObject(BaseObject object)
	{
		return (objects.remove(object.id) == object);
	}
	
	@Override
	public boolean hit()
	{
		boolean result = (type != TileTypes.Floor);
		
		//Relay to objects sitting on this tile
		Iterator<BaseObject> it = objects.values().iterator();
		while (it.hasNext())
			result = result | it.next().hit();
		
		//Spawn dynamic death object
		if (type == TileTypes.Block)
		{
			world.addObject(new ExplodingBlock(world, item), x, y, true);
			setType(TileTypes.Floor);
		}
		
		return  result;
	}
}
