/**
 * QueueContainer.java 
 * Created on 15.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

/**
 * @author Heiko Schmitt
 *
 */
public class QueueContainer
{
	public BaseObject object;
	public MapTile fromTile;
	public boolean keepID;
	
	public QueueContainer(BaseObject object)
	{
		this.object = object;
		this.fromTile = null;
		this.keepID = false;
	}
	
	public QueueContainer(BaseObject object, MapTile fromTile, boolean keepID)
	{
		this.object = object;
		this.fromTile = fromTile;
		this.keepID = keepID;
	}
}
