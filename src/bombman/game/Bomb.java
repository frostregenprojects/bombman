/**
 * Bomb.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import java.util.Iterator;

import bombman.render.Renderer;
import bombman.sound.SoundManager;

/**
 * @author Heiko Schmitt
 *
 */
public class Bomb extends BaseObject
{
	private static final float WALK_RADIUS = 0.45f;

	public Controllable owner;
	public int strength;
	
	private int countdown;
	private boolean exploded;
	
	private int animationFrame;
	private int animationBase;
	private int animTickDelay;
	private int animModuloTest;
	
	private float vx;
	private float vy;
	
	/**
	 * 
	 */
	public Bomb(World world, int countdown, int strength, Controllable owner)
	{
		super(world);
		this.owner = owner;
		this.countdown = countdown;
		this.strength = strength;
		animationBase = 0;
		animationFrame = 0;
		animTickDelay = Math.max(2, FastMath.round(6 * speedFactor));
		animModuloTest = animTickDelay - 1;
		affectsPassable = true;
		isBomb = true;
		exploded = false;
		vx = 0;
		vy = 0;
	}

	@Override
	public void tick(long frame)
	{
		//Explosion countdown
		countdown--;
		if (countdown == 0) explode();
		
		//Check Objects on same tile
		MapTile tile = world.getTile(x, y);
		boolean isDeadly = tile.isDeadly;
		Iterator<BaseObject> it = tile.objects.values().iterator();
		while (it.hasNext() && (!isDeadly))
			isDeadly = isDeadly | it.next().isDeadly;
		if (isDeadly) explode();
		
		if (!exploded)
		{
			//Animation
			if ((frame % animTickDelay) == animModuloTest) animationFrame++;
			if (animationFrame >= 8) animationFrame = 0;
			
			//Move if kicked
			if ((vx != 0) || (vy != 0))
			{
				MapTile tileSelf = world.getTile(x, y);
				MapTile tileNext;
				if (vx != 0)
				{
					//Select right tile
					if (vx > 0) tileNext = world.getTile(x + vx + WALK_RADIUS, y);
					else        tileNext = world.getTile(x + vx - WALK_RADIUS, y);
					
					//Move
					if (tileSelf == tileNext)
					{
						if (!tileSelf.isOccupiedByPlayer)
							x = x + vx;
						else 
							vx = 0;
					}
					else
					{
						if (tileNext.isPassable && !tileNext.isOccupiedByPlayer)
							x = x + vx;
						else
							vx = 0;
					}
				}
				if (vy != 0)
				{
					//Select right tile
					if (vy > 0) tileNext = world.getTile(x, y + vy + WALK_RADIUS);
					else        tileNext = world.getTile(x, y + vy - WALK_RADIUS);
					
					//Move
					if (tileSelf == tileNext)
					{
						if (!tileSelf.isOccupiedByPlayer)
							y = y + vy;
						else 
							vy = 0;
					}
					else
					{
						if (tileNext.isPassable && !tileNext.isOccupiedByPlayer)
							y = y + vy;
						else
							vy = 0;
					}
				}
				if (tileSelf != world.getTile(x, y))
				{
					world.moveObject(this, tileSelf);
				}
			}
		}
	}
	
	public void kick(float vx, float vy)
	{
		if ((this.vx == 0) && (this.vy == 0))
		{
			this.vx = vx;
			this.vy = vy;
		}
	}
	
	public void explode()
	{
		if (!exploded)
		{
			SoundManager.playSound(1);
			exploded = true;
			countdown = 0;
			world.explodeBomb(this);
		}
	}
	
	@Override
	public void render(float advance)
	{
		Renderer.draw(animationBase + animationFrame, 32, 32, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
	}
	
	@Override
	public boolean hit()
	{
		explode();
		return true;
	}
}
