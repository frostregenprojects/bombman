/**
 * Tickable.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

/**
 * @author Heiko Schmitt
 *
 */
public interface Tickable
{
	public void tick(long frame);
}
