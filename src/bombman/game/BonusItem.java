/**
 * BonusItem.java 
 * Created on 13.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class BonusItem extends BaseObject
{
	private int animationBase;
	private int animationFrame;
	private int animTickDelay;
	private int animModuloTest;
	
	public ItemTypes type;
	
	
	public enum ItemTypes
	{
		ExtraBomb,
		FlameExtend,
		Speed,
		Kick,
		PowerBomb,
		GoldenFlameExtend,
		Glove,
		End
	}
	
	/**
	 * @param world
	 */
	public BonusItem(World world, ItemTypes type)
	{
		super(world);
		isItem = true;
		animationFrame = 0;
		animTickDelay = Math.max(2, FastMath.round(6 * speedFactor));
		animModuloTest = animTickDelay - 1;
		
		setType(type);
	}
	
	public void setType(ItemTypes type)
	{
		this.type = type;
		
		switch (type)
		{
			case ExtraBomb: animationBase = 576; break;
			case FlameExtend: animationBase = 578; break;
			case Speed: animationBase = 580; break;
			case Kick: animationBase = 582; break;
			
			default: break;
		}
	}

	@Override
	public void tick(long frame)
	{
		//Animation
		if ((frame % animTickDelay) == animModuloTest) animationFrame++;
		if (animationFrame >= 2) animationFrame = 0;
	}
	
	@Override
	public void render(float advance)
	{
		Renderer.draw(animationBase + animationFrame, 32, 32,Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
	}
	
	@Override
	public boolean hit()
	{
		world.removeObject(this, true);
		return true;
	}
}
