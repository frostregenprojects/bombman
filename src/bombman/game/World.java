/**
 * World.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.Rectangle;

import bombman.application.Settings;
import bombman.game.BonusItem.ItemTypes;
import bombman.game.Explosion.ExplosionType;
import bombman.game.MapTile.TileTypes;
import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class World extends BaseObject
{
	private static final int MAX_DYNAMIC = 100000;
	private static final int MAX_STATIC_DRAWQUEUE = 100000;
	private static final int MAX_DRAWQUEUE = 1000;
	private static final int Z_LEVELS = 10;
	
	/* List for dynamic Objects */
	private LinkedHashMap<Integer, BaseObject> dynamicObjects;	//dynamic objects
	private Queue<Integer> idQueue;								//free unique object id's
	private Queue<QueueContainer> removeQueue;					//deferred removal
	private Queue<QueueContainer> addQueue;						//deferred add
	private Queue<BaseObject>[] drawQueue;						//deferred render (for z levels)
	private Queue<BaseObject> drawLightQueue;					//deferred light render
	private Queue<MapTile> drawStaticQueue;						//static stuff
	
	/* Array for map content */
	private MapTile[] map;
	private int mapWidth;
	private int mapHeight;
	private int mapLength;
	
	/* Scaling */
	private float scale;
	private float translateX;
	private float translateY;
	
	/* Textures for tileSet */
	public int[] tileSet;
	public int[] tileSetNormal;
	
	/* Settings */
	private Settings settings;
	
	/* Game */
	private long tick;
	
	/* Sudden Death */
	public long suddenDeathStartTick;
	
	@SuppressWarnings("unchecked")
	public World(Settings options)
	{
		super(null);
		settings = options;
		
		// Initialize management lists and queues
		dynamicObjects = new LinkedHashMap<Integer, BaseObject>();
		idQueue = new ArrayBlockingQueue<Integer>(MAX_DYNAMIC);
		removeQueue = new ArrayBlockingQueue<QueueContainer>(MAX_DYNAMIC);
		addQueue = new ArrayBlockingQueue<QueueContainer>(MAX_DYNAMIC);
		for (int i = 0; i < MAX_DYNAMIC; i++)
			idQueue.offer(i);
		drawStaticQueue = new ArrayBlockingQueue<MapTile>(MAX_STATIC_DRAWQUEUE);
		drawLightQueue = new ArrayBlockingQueue<BaseObject>(MAX_DRAWQUEUE);
		drawQueue = new ArrayBlockingQueue[Z_LEVELS];
		for (int i = 0; i < Z_LEVELS; i++)
			drawQueue[i] = new ArrayBlockingQueue<BaseObject>(MAX_DRAWQUEUE);
		
		// Set tileSet
		tileSet = new int[TileTypes.End.ordinal()];
		tileSet[TileTypes.Floor.ordinal()] = 512;
		tileSet[TileTypes.Block.ordinal()] = 513;
		tileSet[TileTypes.InnerWall.ordinal()] = 514;
		tileSet[TileTypes.OuterWall.ordinal()] = 515;
		// Set normalMap
		tileSetNormal = new int[TileTypes.End.ordinal()];
		tileSetNormal[TileTypes.Floor.ordinal()] = 640;
		tileSetNormal[TileTypes.Block.ordinal()] = 641;
		tileSetNormal[TileTypes.InnerWall.ordinal()] = 642;
		tileSetNormal[TileTypes.OuterWall.ordinal()] = 643;
		
		// Reduce mapsize to odd values, limit to 7x7 minimum
		mapWidth = Math.max(7, settings.levelWidth - ((settings.levelWidth + 1) % 2));
		mapHeight = Math.max(7, settings.levelHeight - ((settings.levelHeight + 1) % 2));
		// Calculate playable area
		int areaWidth = mapWidth - 2;
		int areaHeight = mapHeight - 2;
		// Calculate circumference
		int u = (2 * (areaWidth-1)) + (2 * (areaHeight-1));
		// Limit max players based on circumference
		settings.playerCount = Math.min(u / 4, settings.playerCount);
		
		// Set scaling values
		scale = ((float)settings.displayWidth / (Renderer.TILESIZE * mapWidth));
		if (scale > ((float)settings.displayHeight / (Renderer.TILESIZE * mapHeight)))
			scale = (float)settings.displayHeight / (Renderer.TILESIZE * mapHeight);
		translateX = (settings.displayWidth / 2) - ((mapWidth * scale * Renderer.TILESIZE) / 2);
		translateY = (settings.displayHeight / 2) - ((mapHeight * scale * Renderer.TILESIZE) / 2);
		
		// Initialize bonus items and probabilities
		ItemTypes[] items = ItemTypes.values();
		ItemTypes[] itemTypes = new ItemTypes[100];
		int maxItem = 100;
		for (int i = 0; i < maxItem; i++)
		{
			if (i < 35) itemTypes[i] = items[0];
			else if (i < 70) itemTypes[i] = items[1];
			else if (i < 90) itemTypes[i] = items[2];
			else itemTypes[i] = items[3];
		}
		
		// Initialize sudden death
		suddenDeathStartTick = BaseObject.secondsToTicks(180);
		suddenDeathAdvanceX = 1;
		suddenDeathAdvanceY = 0;
		suddenDeathBorder = new Rectangle(1, 1, mapWidth - 3, mapHeight - 3);
		suddenDeathX = suddenDeathBorder.getX();
		suddenDeathY = suddenDeathBorder.getY();
		
		//Initialize map
		Random r = new Random();
		mapLength = mapWidth * mapHeight;
		map = new MapTile[mapLength];
		int x,y, tileIndex;
		tileIndex = 0;
		for (y = 0; y < mapHeight; y++)
		{
			for (x = 0; x < mapWidth; x++)
			{
				map[tileIndex] = new MapTile(this, TileTypes.Floor);
				map[tileIndex].x = x;
				map[tileIndex].y = y;
				
				if ((x == 0) || (y == 0) || (x == (mapWidth - 1)) || (y == (mapHeight - 1)))
					map[tileIndex].setType(TileTypes.OuterWall);
				else if (((x % 2) == 0) && ((y % 2) == 0))
					map[tileIndex].setType(TileTypes.InnerWall);
				else if (!(((x > 3) && (y > 3) && (x < mapWidth - 4) && (y < mapHeight - 4))) || (r.nextInt(100) > 30))
				{
					map[tileIndex].setType(TileTypes.Block);
					
					//Add Bonus Item
					if (r.nextInt(100) > 65)
						map[tileIndex].setBonusItem(new BonusItem(this, itemTypes[r.nextInt(maxItem)]));
				}	
				
				tileIndex++;
			}
		}
		//Add Players
		int offset = 1; //start at x/y = 1
		int playerDist = (int)((float)u / settings.playerCount);
		int nextDist = 1;
		int playersSet = 0;
		x = offset;
		y = offset;
		for (x = offset; x < areaWidth; x++)
		{
			nextDist--;
			if (nextDist <= 0)
			{
				addPlayer(x, y, playersSet);
				playersSet++;
				nextDist = playerDist;
			}
		}
		
		for (y = offset; y < areaHeight; y++)
		{
			nextDist--;
			if (nextDist <= 0)
			{
				addPlayer(x, y, playersSet);
				playersSet++;
				nextDist = playerDist;
			}
		}
		for (x = areaWidth; x > offset; x--)
		{
			nextDist--;
			if (nextDist <= 0)
			{
				addPlayer(x, y, playersSet);
				playersSet++;
				nextDist = playerDist;
			}
		}
		for (y = areaHeight; y > offset; y--)
		{
			nextDist--;
			if (nextDist <= 0)
			{
				addPlayer(x, y, playersSet);
				playersSet++;
				nextDist = playerDist;
			}
		}
	}

	private void addPlayer(int x, int y, int playerID)
	{
		//Carve out room
		makeRoomForPlayer(x, y);
		
		//Add player object
		Player player = new Player(this, playerID);
		addObject(player, x, y, false);
		
		//Register controls
		settings.keymap.registerControllable(playerID, player);
	}
	
	private void makeRoomForPlayer(int x, int y)
	{
		/*
		 * Each Player needs at least a 2x2 space.
		 * 
		 * Target of this function will be to create an edge.
		 * Like:
		 * ** or ** or *  or  *
		 * *     *     **    **
		 * 
		 * Algorithm:
		 * Scan area around player for first InnerWall.
		 * Create a 2x2 square.
		 * clear all blocks from this square
		 * 
		 */
		
		//Find first inner wall
		int xi = 0;
		int yi = y - 1;
		boolean found = false;
		while ((yi < (y + 2)) && (!found))
		{
			xi = x - 1;
			while ((xi < (x + 2)) && (!found))
			{
				if (getTile(xi, yi).type == TileTypes.InnerWall) found = true;
				if (!found) xi++;
			}
			if (!found) yi++;
		}
		
		//Expand to 2x2 area
		if (x == xi) x++;
		if (y == yi) y++;
		
		//Swap if necessary
		int tmp;
		if (x > xi)
		{
			tmp = xi;
			xi = x;
			x = tmp;
		}
		if (y > yi)
		{
			tmp = yi;
			yi = y;
			y = tmp;
		}

		//Clear all Blocks
		MapTile tile;
		for (int y2 = y; y2 <= yi; y2++)
		{
			for (int x2 = x; x2 <= xi; x2++)
			{
				tile = getTile(x2, y2);
				if (tile.type == TileTypes.Block)
				{
					tile.setType(TileTypes.Floor);
					tile.setBonusItem(null);
				}
			}
		}
	}
	
	@Override
	public void tick(long frame)
	{
		tick = frame;
		
		// Iterate over dynamic objects and call .tick()
		Iterator<BaseObject> it = dynamicObjects.values().iterator();
		while (it.hasNext())
			it.next().tick(frame);
		
		// Process sudden death
		if (tick - suddenDeathStartTick >= 0)
		{
			processSuddenDeath(tick - suddenDeathStartTick);
		}
		
		// Process Remove and add Queue
		processQueues();
		
		// TEST: Spawn Bomb
//		Random r = new Random();
//		int x = r.nextInt(mapWidth);
//		int y = r.nextInt(mapHeight);
//		if (map[x + (y * mapWidth)].type == TileTypes.Floor)
//			if (map[x + (y * mapWidth)].isPassable) addObject(new Bomb(this, 200, 5, null), x, y, false);
	}
	
	private boolean advanceSuddenDeathTile()
	{
		if (suddenDeathBorder.getHeight() - suddenDeathBorder.getY() <= 4 ||
				suddenDeathBorder.getWidth() - suddenDeathBorder.getX() <= 4)
		{
			return false;
		}
		suddenDeathX += suddenDeathAdvanceX;
		suddenDeathY += suddenDeathAdvanceY;
		if (suddenDeathX > suddenDeathBorder.getWidth() && suddenDeathAdvanceX == 1)
		{
			suddenDeathAdvanceX = 0;
			suddenDeathAdvanceY = 1;
			suddenDeathBorder.setY(suddenDeathBorder.getY() + 1);
		}
		if (suddenDeathY > suddenDeathBorder.getHeight() && suddenDeathAdvanceY == 1)
		{
			suddenDeathAdvanceX = -1;
			suddenDeathAdvanceY = 0;
			suddenDeathBorder.setWidth(suddenDeathBorder.getWidth() - 1);
		}
		if (suddenDeathX <= suddenDeathBorder.getX() && suddenDeathAdvanceX == -1)
		{
			suddenDeathAdvanceX = 0;
			suddenDeathAdvanceY = -1;
			suddenDeathBorder.setHeight(suddenDeathBorder.getHeight() - 1);
		}
		if (suddenDeathY <= suddenDeathBorder.getY() && suddenDeathAdvanceY == -1)
		{
			suddenDeathAdvanceX = 1;
			suddenDeathAdvanceY = 0;
			suddenDeathBorder.setX(suddenDeathBorder.getX() + 1);
		}
		if (suddenDeathBorder.getHeight() - suddenDeathBorder.getY() <= 4 ||
				suddenDeathBorder.getWidth() - suddenDeathBorder.getX() <= 4)
		{
			return false;
		}
		return true;
	}
	
	private int suddenDeathX;
	private int suddenDeathY;
	private int suddenDeathAdvanceX;
	private int suddenDeathAdvanceY;
	private Rectangle suddenDeathBorder;
	private void processSuddenDeath(long suddenDeathTick)
	{
		if (suddenDeathTick % ((BaseObject.secondsToTicks(1) / 2)) != 0)
		{
			return;
		}
		
		// Skip inner walls
		while (getTile(suddenDeathX, suddenDeathY).type == TileTypes.InnerWall)
		{
			if (advanceSuddenDeathTile() == false)
			{
				return;
			}
		}
		addObject(new SuddenDeathBlock(this), suddenDeathX, suddenDeathY, true);
		advanceSuddenDeathTile();
	}
	
	private void renderMapTile(int tileIndex)
	{
		MapTile tile = map[tileIndex];
		
		//Render tile
		Renderer.draw(tile.textureID, 32, 32, Renderer.TILESIZE_H + (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, true);
		
		//Render shadow, if tile above is a Wall
		if ((tileIndex >= mapWidth) && (tile.type.equals(TileTypes.Floor)))
		{
			MapTile tileAbove = map[tileIndex - mapWidth];
			if (tileAbove.type.equals(TileTypes.InnerWall))
				Renderer.draw(524, 32, 32, Renderer.TILESIZE_H + (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, true);
			else if (tileAbove.type.equals(TileTypes.OuterWall))
				Renderer.draw(525, 32, 32, Renderer.TILESIZE_H + (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, true);
		}
		
		//Debug rendering
		if (settings.debugRenderPassable && !tile.isPassable) Renderer.drawString("N", 8, (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, 1, 1, 1, true);
		if (settings.debugRenderOccupiedByBomb && tile.isOccupiedByBomb) Renderer.drawString(" O", 8, (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, 1, 1, 1, true);
		if (settings.debugRenderOccupiedByPlayer && tile.isOccupiedByPlayer) Renderer.drawString("  P", 8, (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, 1, 1, 1, true);
	}
	
	private void renderMapTileNormal(int tileIndex)
	{
		MapTile tile = map[tileIndex];
		
		//Render tile
		Renderer.draw(tile.normalTexID, 32, 32, Renderer.TILESIZE_H + (tile.x * Renderer.TILESIZE), Renderer.TILESIZE_H + (tile.y * Renderer.TILESIZE), 0, 0, true);
	}
	
	private void renderPath_AlwaysRerenderStatic(float advance)
	{
		//Scale and translate for maximum screen usage
		GL11.glPushMatrix();
		GL11.glTranslatef(translateX, translateY, 0);
		GL11.glScalef(scale, scale, 1);
		
		//Render Background
		for (int i = 0; i < mapLength; i++)
			renderMapTile(i);
		
		//Render Dynamic content
		renderDynamicContent(advance);
		
		GL11.glPopMatrix();
	}
	
	private void renderPath_staticBackground_noShader(float advance)
	{
		//Scale and translate for maximum screen usage
		GL11.glPushMatrix();
		GL11.glTranslatef(translateX, translateY, 0);
		GL11.glScalef(scale, scale, 1);
		
		//Render static content into texture
		Renderer.enableFBO(Renderer.backgroundTextureID);
		while (drawStaticQueue.peek() != null)
			renderMapTile(getTileIndex(drawStaticQueue.remove()));
		Renderer.disableFBO();

		//render static texture into other FBO
		GL11.glPopMatrix();
		Renderer.drawFullscreenTexture(Renderer.backgroundTextureID);
		
		//Scale again for dynamic content
		GL11.glPushMatrix();
		GL11.glTranslatef(translateX, translateY, 0);
		GL11.glScalef(scale, scale, 1);
		
		//Render Dynamic content
		renderDynamicContent(advance);
		
		GL11.glPopMatrix();
	}
	
	private void renderPath_staticBackground_Shaders(float advance)
	{
		//Scale and translate for maximum screen usage
		GL11.glPushMatrix();
		GL11.glTranslatef(translateX, translateY, 0);
		GL11.glScalef(scale, scale, 1);
		
		//Render static content into texture (RGB)
		Renderer.enableFBO(Renderer.backgroundTextureID);
		Iterator<MapTile> it = drawStaticQueue.iterator();
		while (it.hasNext())
			renderMapTile(getTileIndex(it.next()));
		//Render static content into texture (Normal)
		Renderer.enableFBO(Renderer.normalTextureID);
		it = drawStaticQueue.iterator();
		while (it.hasNext())
			renderMapTileNormal(getTileIndex(it.next()));
		Renderer.disableFBO();
		
		//Empty static queue
		drawStaticQueue.clear();

		//render static texture into other FBO
		GL11.glPopMatrix();
		Renderer.enableFBO(Renderer.fullscreenBufferTextureID);
		
		//Bind backbufferTex
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, Renderer.backgroundTextureID);
		// Notify renderer about our texture change
		Renderer.resetBoundTexture();
		
		//Bind normalMap
		GL13.glActiveTexture(GL13.GL_TEXTURE1);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, Renderer.normalTextureID);
		
		Renderer.bindShader("light");
		
		//Set light position
		float time = (float)(tick % 360) / 180 * (float)Math.PI;
		GL20.glUniform2f(Renderer.getUniformLocation("lightPos"), 0.5f + ((float)Math.sin(time) / 4), 0.5f + ((float)Math.cos(time) / 4));
		GL20.glUniform2f(Renderer.getUniformLocation("screenSize"), settings.displayWidth, settings.displayHeight);
		
		//Set textures
		GL20.glUniform1i(Renderer.getUniformLocation("backBufferTex"), 0);
		GL20.glUniform1i(Renderer.getUniformLocation("normalMapTex"), 1);
		
		//Render Quad
		Renderer.drawFullscreenTexture(-1);
		Renderer.unbindShader();
		
		//Disable texture
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		
		//Scale again for dynamic content
		GL11.glPushMatrix();
		GL11.glTranslatef(translateX, translateY, 0);
		GL11.glScalef(scale, scale, 1);
		
		//Render Dynamic content
		Renderer.bindShader("lightObject");
		GL20.glUniform2f(Renderer.getUniformLocation("lightPos"), 0.5f + ((float)Math.sin(time) / 4), 0.5f + ((float)Math.cos(time) / 4));
		GL20.glUniform2f(Renderer.getUniformLocation("screenSize"), settings.displayWidth, settings.displayHeight);
		renderDynamicContent(advance);
		Renderer.unbindShader();
		Renderer.disableFBO();
		
		GL11.glPopMatrix();
		
		//Draw result
		Renderer.drawFullscreenTexture(Renderer.fullscreenBufferTextureID);
	}
	
	private void renderDynamicContent(float advance)
	{
		//render dynamic
		BaseObject obj;
		Iterator<BaseObject> it = dynamicObjects.values().iterator();
		while (it.hasNext())
		{
			obj = it.next();
			if (obj.zLevel < 0) obj.render(advance);
			else sortedDraw(obj, obj.zLevel);
			if (obj.emitsLight()) drawLight(obj);
		}
		
		//render dynamic with different Z level "sortedDraw()"
		for (int i = 0; i < Z_LEVELS; i++)
		{
			while (drawQueue[i].peek() != null)
			{
				drawQueue[i].remove().render(advance);
			}
		}
		
		//Render lights "drawLight()"
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
		while (drawLightQueue.peek() != null)
		{
			drawLightQueue.remove().renderLight(advance);
		}
		GL11.glColor4f(1, 1, 1, 1);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	@Override
	public void render(float advance)
	{
		//Just select correct renderpath here
		if (settings.debugAlwaysRenderStatic)
		{
			renderPath_AlwaysRerenderStatic(advance);
		}
		else
		{
			if (settings.useShaders)
				renderPath_staticBackground_Shaders(advance);
			else
				renderPath_staticBackground_noShader(advance);
		}
	}
	
	private void processQueues()
	{
		QueueContainer qc;
		
		//Process Remove Queue
		while (removeQueue.peek() != null)
		{
			qc = removeQueue.remove();
			removeObject(qc.object, qc.fromTile, qc.keepID, false);
		}
		
		//Process Add Queue
		while (addQueue.peek() != null)
		{
			qc = addQueue.remove();
			addObject(qc.object, qc.object.x, qc.object.y, false);
		}
	}
	
	public void sortedDraw(BaseObject object, int zLevel)
	{
		drawQueue[zLevel].offer(object);
	}
	
	public void drawLight(BaseObject object)
	{
		drawLightQueue.offer(object);
	}
	
	public void drawToStatic(MapTile object)
	{
		if (!settings.debugAlwaysRenderStatic) drawStaticQueue.offer(object);
	}
	
	public void addObject(BaseObject object, float x, float y, boolean queue)
	{
		object.x = x;
		object.y = y;
		//Immediately update tile properties
		if (object.affectsPassable) getTile(object.x, object.y).isPassable = false;
		if (object.isBomb) getTile(object.x, object.y).isOccupiedByBomb = true;
		if (object.isPlayer) getTile(object.x, object.y).isOccupiedByPlayer = true;
		
		if (queue)
		{
			addQueue.offer(new QueueContainer(object));
		}
		else
		{
			if (object.id < 0) object.id = idQueue.remove();
			getTile(object.x, object.y).addObject(object);
			dynamicObjects.put(object.id, object);
		}
	}
	
	public void removeObject(BaseObject object, boolean queue)
	{
		removeObject(object, getTile(object.x, object.y), false, queue);
	}
	
	private void removeObject(BaseObject object, MapTile fromTile, boolean keepID, boolean queue)
	{
		if (queue)
		{
			removeQueue.offer(new QueueContainer(object, fromTile, keepID));
		}
		else
		{
			if (!fromTile.removeObject(object)) System.out.println("tile.remove failed. Object not found: " + object.getClass().getSimpleName());
			if (dynamicObjects.remove(object.id) == null) System.out.println("dynamic.remove failed. Object not found: " + object.getClass().getSimpleName());
			if (!keepID) idQueue.offer(object.id);
			if (object.affectsPassable) fromTile.isPassable = fromTile.type == TileTypes.Floor;
			if (object.isBomb) fromTile.isOccupiedByBomb = false;
			if (object.isPlayer) fromTile.isOccupiedByPlayer = false;
		}
	}
	
	public void moveObject(BaseObject object, MapTile oldTile)
	{
		removeObject(object, oldTile, true, true);
		addObject(object, object.x, object.y, true);
	}
	
	public boolean isTileClear(float x, float y)
	{
		return getTile(x, y).isPassable;
	}
	
	public void explodeBomb(Bomb bomb)
	{
		//Notify owner that bomb has exploded
		if (bomb.owner != null) bomb.owner.notifyBombExploded();
		
		//Iterate over map, create explosion particles
		int x = FastMath.round(bomb.x);
		int y = FastMath.round(bomb.y);
		
		Explosion explosion = new Explosion(this, ExplosionType.Center, bomb.owner);
		explosion.setLightStrength(bomb.strength);
		addObject(explosion, x, y, true);
		getTile(x, y).hit();
		
		int xi = x - 1;
		while (((x - xi) <= bomb.strength) && (!getTile(xi, y).hit()))
		{
			if ((x - xi) == bomb.strength)
				addObject(new Explosion(this, ExplosionType.EndLeft, bomb.owner), xi, y, true);
			else
				addObject(new Explosion(this, ExplosionType.Horizontal, bomb.owner), xi, y, true);
			xi--;
		}
		xi = x + 1;
		while (((xi - x) <= bomb.strength) && (!getTile(xi, y).hit()))
		{
			if ((xi - x) == bomb.strength)
				addObject(new Explosion(this, ExplosionType.EndRight, bomb.owner), xi, y, true);
			else
				addObject(new Explosion(this, ExplosionType.Horizontal, bomb.owner), xi, y, true);
			xi++;
		}
		int yi = y - 1;
		while (((y - yi) <= bomb.strength) && (!getTile(x, yi).hit()))
		{
			if ((y - yi) == bomb.strength)
				addObject(new Explosion(this, ExplosionType.EndUp, bomb.owner), x, yi, true);
			else
				addObject(new Explosion(this, ExplosionType.Vertical, bomb.owner), x, yi, true);
			yi--;
		}
		yi = y + 1;
		while (((yi - y) <= bomb.strength) && (!getTile(x, yi).hit()))
		{
			if ((yi - y) == bomb.strength)
				addObject(new Explosion(this, ExplosionType.EndDown, bomb.owner), x, yi, true);
			else
				addObject(new Explosion(this, ExplosionType.Vertical, bomb.owner), x, yi, true);
			yi++;
		}
		
		//Remove this object
		removeObject(bomb, true);
	}
	
	public MapTile getTile(float x, float y)
	{
		return map[FastMath.round(x) + (FastMath.round(y) * mapWidth)];
	}
	
	public int getTileIndex(MapTile tile)
	{
		return FastMath.round(tile.x) + (FastMath.round(tile.y) * mapWidth);
	}
}
