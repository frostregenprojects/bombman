/**
 * BaseObject.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.render.Renderable;

/**
 * @author Heiko Schmitt
 *
 */
public abstract class BaseObject implements Renderable, Tickable, Hittable
{
	public World world;
	
	/* Unique ID of this object */
	public int id;
	
	public float x;
	public float y;
	public int zLevel;				//Determines Layer to draw this Object in
	
	public boolean affectsPassable; //If object affects MapTile.passable
	public boolean isBomb;			//If this is a Bomb
	public boolean isDeadly;		//If this item hits other stuff on same tile
	public boolean isItem;			//If this is is an Item
	public boolean isPlayer;		//If this is a player
	
	/**
	 * Multiply this factor with 60, to now how many ticks one second is.
	 */
	public static float speedFactor;//Speed Factor. determined by set gameFPS
	
	/**
	 * 
	 */
	public BaseObject(World world)
	{
		this.world = world;
		id = -1;
		affectsPassable = false;
		isBomb = false;
		isDeadly = false;
		isItem = false;
		this.zLevel = -1;
	}

	public static long ticksToSeconds(long tick)
	{
		return (long)(tick / speedFactor / 60);
	}
	
	public static long secondsToTicks(long seconds)
	{
		return (long)(seconds * speedFactor * 60);
	}
	
	@Override
	public void render(float advance)
	{
		
	}

	@Override
	public void tick(long frame)
	{
		
	}
	
	@Override
	public boolean hit()
	{
		return false;
	}
	
	@Override
	public boolean emitsLight()
	{
		return false;
	}
	
	@Override
	public void renderLight(float advance)
	{
		
	}
}
