/**
 * ExplodingWall.java 
 * Created on 11.05.2009
 * by Heiko Schmitt
 */
package bombman.game;

import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class ExplodingBlock extends BaseObject
{
	private int animationFrame;
	private int animationBase;
	private int animTickDelay;
	private int animModuloTest;
	
	private BonusItem item;
	
	/**
	 * 
	 */
	public ExplodingBlock(World world, BonusItem item)
	{
		super(world);
		this.item = item;
		
		affectsPassable = true;
		animationFrame = 0;
		animationBase = 516;
		animTickDelay = Math.max(2, FastMath.round(6 * speedFactor));
		animModuloTest = animTickDelay - 1;
	}

	@Override
	public void tick(long frame)
	{
		//Animation
		if ((frame % animTickDelay) == animModuloTest) animationFrame++;
		if (animationFrame >= 8)
		{
			world.removeObject(this, true);
			if (item != null) world.addObject(item, x, y, true);
		}
	}

	@Override
	public void render(float advance)
	{
		Renderer.draw(animationBase + animationFrame, 32, 32, Renderer.TILESIZE_H + (x * Renderer.TILESIZE), Renderer.TILESIZE_H + (y * Renderer.TILESIZE), 0, 0, true);
	}
	
	@Override
	public boolean hit()
	{
		return true;
	}
}
