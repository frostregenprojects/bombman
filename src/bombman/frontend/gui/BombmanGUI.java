/**
 * BombmanGUI.java 
 * Created on 07.05.2009
 * by Heiko Schmitt
 */
package bombman.frontend.gui;

import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import bombman.application.Settings;
import bombman.debug.Debug;
import bombman.game.BaseObject;
import bombman.game.UserInterface;
import bombman.render.FPSMeter;
import bombman.render.Renderer;
import bombman.sound.SoundManager;

/**
 * @author Heiko Schmitt
 * 
 */
public class BombmanGUI
{
	private boolean terminate;
	private Renderer renderer;
	private SoundManager soundManager;
	private Settings settings;
	
	/**
	 * 
	 */
	public BombmanGUI(String[] args)
	{
		Debug.setGlobalDebugLevel(5);
		parse(args);
		
		renderer = new Renderer(settings);
		soundManager = new SoundManager(settings);
	}

	private void parse(String[] args)
	{
		settings = new Settings();
		
		//TODO parse...
	}

	public void run()
	{
		init();
		gameLoop();
		cleanup();
	}
	
	private void gameLoop()
	{
		terminate = false;
		final FPSMeter fps = new FPSMeter();
		long gameFrame = 0;
		
		// Create UserInterface
		BaseObject.speedFactor = settings.gameFPS / 60f;
		final UserInterface ui = new UserInterface(settings);

		while (!terminate && !Display.isCloseRequested())
		{
			// Title with info
			if ((gameFrame % settings.gameFPS) == 0)
				Display.setTitle("FPS: " + fps.getFps() + "  Frame: " + gameFrame);
			
			// check input
			processInput();
			
			// process world
			ui.tick(gameFrame);
			
			// render
			renderer.frameStart();
			ui.render(0);
			renderer.frameEnd();
			
			//Update Frame data
			fps.tick();
			gameFrame++;
		}
	}
	
	private void init()
	{
		renderer.init();
		soundManager.init();
		settings.keymap.init();
	}
	
	private void cleanup()
	{
		settings.keymap.dispose();
		soundManager.dispose();
		renderer.dispose();
	}
	
	private void processInput()
	{
		int eventID = 0;
		while (Keyboard.next())
		{
			eventID = settings.keymap.getEventID(0, Keyboard.getEventKey(), Keyboard.getEventKeyState() ? 1 : 0, false);
			
			//Check Keypresses:
			if (eventID == settings.keymap.getEventID(0, Keyboard.KEY_ESCAPE, 1, false)) terminate = true;
			else settings.keymap.processEvent(eventID);
		}
		Controller controller;
		while (Controllers.next())
		{ 
			controller = Controllers.getEventSource();
			int controlIndex = Controllers.getEventControlIndex();			
			if (Controllers.isEventButton())
				eventID = settings.keymap.getEventID(controller.getIndex(), controlIndex, controller.isButtonPressed(controlIndex) ? 1 : 0, false);
			else if (Controllers.isEventAxis())
				eventID = settings.keymap.getEventID(controller.getIndex(), controlIndex, (int)(controller.getAxisValue(controlIndex) * 10), true);
			//Process Event
			settings.keymap.processEvent(eventID);
		}
	}

}
