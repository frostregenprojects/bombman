/**
 * Application.java 
 * Created on 07.05.2009
 * by Heiko Schmitt
 */
package bombman.application;

import bombman.frontend.gui.BombmanGUI;

/**
 * @author Heiko Schmitt
 *
 */
public class Application
{
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		BombmanGUI gui = new BombmanGUI(args);
		gui.run();
	}
}
