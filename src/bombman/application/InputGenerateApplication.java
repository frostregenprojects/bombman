/**
 * InputGenerateApplication.java 
 * Created on 16.05.2009
 * by Heiko Schmitt
 */
package bombman.application;

import java.io.File;
import java.io.PrintStream;

import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import bombman.debug.Debug;
import bombman.input.KeyMapping.Commands;

/**
 * @author Heiko Schmitt
 *
 */
public class InputGenerateApplication
{
	private static PrintStream out;
	private static Settings settings;
	private static boolean quit;
	private static int currentPlayer = 0;
	private static int currentCommand = 0;
	private static Commands[] cmds = 
	{
		Commands.BOMB_BUTTON_PRESS,
		Commands.SPECIAL_BUTTON_PRESS,
		Commands.WALK_X_LEFT,
		Commands.WALK_X_RIGHT,
		Commands.WALK_Y_UP,
		Commands.WALK_Y_DOWN
	};
	
	public static void main(String[] args)
	{
		try
		{
			Debug.setGlobalDebugLevel(5);
			System.out.println("Hit ESC to Quit.");
			System.out.println("Hit the following buttons for every player in this order:");
			System.out.println("BOMB, SPECIAL, LEFT, RIGHT, UP, DOWN <NEXT PLAYER and REPEAT>");
			System.out.println("The output is in keymap_new.ini");
			
			Display.setDisplayMode(new DisplayMode(100, 100));
			Display.create();
			
			settings = new Settings();
			settings.keymap.init();
			
			File outFile = new File("keymap_new.ini");
			if (outFile.exists()) outFile.delete();
			out = new PrintStream(outFile);
			
			printWantedKeypress();
			quit = false;
			while (!quit)
			{
				processInput();
			}
			
			out.close();
			settings.keymap.dispose();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private static void printWantedKeypress()
	{
		System.out.println("Player " + currentPlayer + " press " + cmds[currentCommand].name());
	}
	
	private static void outputLine(int eventID)
	{
		out.println(eventID + "\t" + cmds[currentCommand].name() + "\t" + currentPlayer);
		
		currentCommand++;
		if (currentCommand >= cmds.length)
		{
			currentCommand = 0;
			currentPlayer++;
		}
		
		printWantedKeypress();
	}
	
	private static void processInput()
	{
		Display.update();
		
		int eventID = 0;
		while (Keyboard.next())
		{
			eventID = settings.keymap.getEventID(0, Keyboard.getEventKey(), Keyboard.getEventKeyState() ? 1 : 0, false);
			
			//Check Keypresses:
			if (eventID == settings.keymap.getEventID(0, Keyboard.KEY_ESCAPE, 1, false)) quit = true;
			else if (eventID != settings.keymap.getReleaseEventID(eventID)) outputLine(eventID);
		}
		Controller controller;
		while (Controllers.next())
		{ 
			controller = Controllers.getEventSource();
			int controlIndex = Controllers.getEventControlIndex();			
			if (Controllers.isEventButton())
				eventID = settings.keymap.getEventID(controller.getIndex(), controlIndex, controller.isButtonPressed(controlIndex) ? 1 : 0, false);
			else if (Controllers.isEventAxis())
				eventID = settings.keymap.getEventID(controller.getIndex(), controlIndex, (int)(controller.getAxisValue(controlIndex) * 10), true);
			//Process Event
			if (eventID != settings.keymap.getReleaseEventID(eventID)) outputLine(eventID);
		}
	}
}
