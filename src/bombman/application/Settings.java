/**
 * Settings.java 
 * Created on 08.05.2009
 * by Heiko Schmitt
 */
package bombman.application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import bombman.debug.Debug;
import bombman.debug.Debug.Area;
import bombman.input.KeyMapping;

/**
 * @author Heiko Schmitt
 *
 */
public class Settings
{
	private static String settingsFile = "settings.ini";
	
	/* Rendering */
	public int displayWidth = 1024;
	public int displayHeight = 768;
	public boolean nonPowerOfTwoSupported = false;
	public int displayBpp = 32;
	public int displayHz = 60;
	public boolean fullscreen = false;
	public int gameFPS = 60;
	public boolean vSync = true;
	public boolean filterTextures = true;
	public boolean useShaders = true;
	//TODO:
	public int maxLightRadius = 20;
	public boolean bombLighting = true;
	
	/* Controller */
	public KeyMapping keymap = new KeyMapping();
	public int playerCount = 4;
	public int levelWidth = 15;
	public int levelHeight = 15;
	
	/* Sounds */
	public int maxSounds = 14;
	public int maxMusic = 2; //16 sources together
	public boolean playMusic = true;
	
	/* Debug */
	public boolean debugAlwaysRenderStatic = false;
	public boolean debugRenderPassable = false;
	public boolean debugRenderOccupiedByBomb = false;
	public boolean debugRenderOccupiedByPlayer = false;
	
	/**
	 * 
	 */
	public Settings()
	{
		loadFromFile(new File(settingsFile));
	}

	private void loadFromFile(File settingsFile)
	{
		if (settingsFile.exists())
		{
			try
			{
				//Read in Keymap
				BufferedReader br = new BufferedReader(new FileReader(settingsFile));
				String line = br.readLine();
				String[] splittedLine;
				int currentLine = 1;
				while (line != null)
				{
					splittedLine = line.split("\t");
					if (splittedLine.length == 2)
					{
						try
						{
							setSetting(splittedLine[0], splittedLine[1]);
						}
						catch (NumberFormatException e)
						{
							Debug.out(Area.Input, 3, "Error parsing setting line, ignoring line: " + currentLine);
						}
					}
					line = br.readLine();
					currentLine++;
				}
				br.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private void setSetting(String settingName, String settingValue) throws NumberFormatException
	{
		//Integer Settings
		     if (settingName.equalsIgnoreCase("displayWidth")) displayWidth = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("displayHeight")) displayHeight = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("displayBpp")) displayBpp = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("displayHz")) displayHz = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("gameFPS")) gameFPS = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("playerCount")) playerCount = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("levelWidth")) levelWidth = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("levelHeight")) levelHeight = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("maxSounds")) maxSounds = Integer.parseInt(settingValue);
		else if (settingName.equalsIgnoreCase("maxMusic")) maxMusic = Integer.parseInt(settingValue);
		
		//Boolean Settings
		else if (settingName.equalsIgnoreCase("fullscreen")) fullscreen = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("vSync")) vSync = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("filterTextures")) filterTextures = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("debugAlwaysRenderStatic")) debugAlwaysRenderStatic = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("debugRenderPassable")) debugRenderPassable = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("debugRenderOccupiedByBomb")) debugRenderOccupiedByBomb = Boolean.parseBoolean(settingValue);     
		else if (settingName.equalsIgnoreCase("debugRenderOccupiedByPlayer")) debugRenderOccupiedByPlayer = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("useShaders")) useShaders = Boolean.parseBoolean(settingValue);
		else if (settingName.equalsIgnoreCase("playMusic")) playMusic = Boolean.parseBoolean(settingValue);
		
	}
}
