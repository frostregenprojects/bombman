/**
 * GameMenu.java 
 * Created on 17.02.2010
 * by Heiko Schmitt
 */
package bombman.menu;

import bombman.game.BaseObject;
import bombman.game.Tickable;
import bombman.game.World;
import bombman.render.Renderable;
import bombman.render.Renderer;

/**
 * @author Heiko Schmitt
 *
 */
public class GameMenu implements Renderable, Tickable
{
	private World world;
	
	private long tick;
	
	/**
	 * 
	 */
	public GameMenu()
	{
		
	}
	
	public World getWorld()
	{
		//Returns freshly constructed world
		return world;
	}
	
	public void setWorld(World world)
	{
		//TODO update menus?
		this.world = world;
	}
	
	/* (non-Javadoc)
	 * @see bombman.render.Renderable#render(float)
	 */
	@Override
	public void render(float advance)
	{
		// Render remaining game time
		// Process Sudden Death
		long ticksUntilSuddenDeath = world.suddenDeathStartTick - tick;
		if (ticksUntilSuddenDeath < BaseObject.secondsToTicks(30) &&
				ticksUntilSuddenDeath > 0)
		{
			Renderer.drawString(Long.toString(BaseObject.ticksToSeconds(ticksUntilSuddenDeath)), 32, 32, 16, 0, 0, 1f, 1f, 1f, true);
		}
	}

	/* (non-Javadoc)
	 * @see bombman.game.Tickable#tick(long)
	 */
	@Override
	public void tick(long frame)
	{
		// Store current frame
		tick = frame;
	}

	@Override
	public boolean emitsLight()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void renderLight(float advance)
	{
		// TODO Auto-generated method stub
		
	}

}
