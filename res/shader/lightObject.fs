uniform sampler2D backBufferTex;	//TMU0
uniform vec2 lightPos;
uniform vec2 screenSize;

void main()
{
	vec3 lPos = vec3(lightPos, 0.0);
	vec3 pPos = vec3(gl_FragCoord.xy / screenSize, 1.0);
	float dist = distance(lPos, pPos);
	vec4 texColor = texture2D(backBufferTex, gl_TexCoord[0].st);
	float attenuation = clamp(1.0 - sqrt(dist / 10.0), 0.0, 1.0);

	gl_FragColor = vec4((texColor.rgb * gl_Color.rgb) * attenuation, texColor.a * gl_Color.a);
}