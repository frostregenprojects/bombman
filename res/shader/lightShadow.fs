uniform sampler2D backBufferTex;	//TMU0
uniform vec2 lightPos;
uniform vec2 screenSize;

void main()
{
	vec3 lPos = vec3(lightPos, 0.0);
	vec3 pPos = vec3(gl_FragCoord.xy / screenSize, 1.0);
	float dist = distance(lPos, pPos);
	vec4 texColor = texture2D(backBufferTex, gl_TexCoord[0].st);
	float attenuation = clamp(1.0 - sqrt(dist / 10.0), 0.0, 1.0);

	gl_FragColor = vec4(texColor.rgb * attenuation, texColor.a);
	
	
	/*
	Basic idea:
	We supply the shader with an objectMap. Basically a grayscale transparency image which
	defines how many light shines through a given pixel. (attenuation can be simulated with this too...)
	
	Lights will be supplied in an array.
	(Maybe use a texture, if arrays do not work...)
	
	
	After this we iterate from current pixel position towards the light. (reduce light amount every step)
	if we reach the light with some light available, this will be the lightamount for this pixel.
	
	Calculate maximum distance, to discard far away lightsource immediately.
	
	
	
	
	*/
}