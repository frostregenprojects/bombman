uniform sampler2D backBufferTex;	//TMU0
uniform sampler2D normalMapTex;	//TMU1
uniform vec2 lightPos;
uniform vec2 screenSize;

const vec3 ambient = vec3(0.5, 0.5, 0.5);
const vec3 lightDiffuse = vec3(0.5, 0.5, 0.5);
const vec3 lightSpecular = vec3(1.0, 1.0, 1.0);

const vec3 view = vec3(0.0, 0.0, 1.0);

void main()
{
	vec3 lPos = vec3(lightPos, 0.0);
	vec3 pPos = vec3(gl_FragCoord.xy / screenSize, 1.0);
	float dist = distance(lPos, pPos);
	vec3 texColor = texture2D(backBufferTex, gl_TexCoord[0].st).rgb;
	float attenuation = clamp(1.0 - sqrt(dist / 10.0), 0.0, 1.0);

	//Calculate basic light value, based on distance
	vec3 lightVec = normalize(pPos - lPos);
	vec3 bumpMod = normalize(texture2D(normalMapTex, gl_TexCoord[0].st).xyz - 0.5);
	vec3 diffuse = lightDiffuse * clamp(pow(dot(lightVec, bumpMod), 4.0), 0.01, 1.0);
	vec3 specular = lightSpecular * clamp(pow(dot(reflect(-lightVec, bumpMod), view), 8.0), 0.01, 1.0);

	gl_FragColor = vec4(((ambient * texColor) + (diffuse * texColor) * attenuation) + specular, 1.0);
}